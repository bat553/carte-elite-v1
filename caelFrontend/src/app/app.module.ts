import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule  } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import {ScrollEventModule} from "ngx-scroll-event";
import {NgxSpinnerModule} from "ngx-spinner";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginComponent } from './components/login/login.component';
import { AuthentificationSystemService } from './services/authentification-system.service';
import { AdminComponent } from './components/admin/admin.component';
import { AuthGuard } from './auth.guard';
import { LogoutComponent } from "./components/logout/logout.component";
import { PwdchangeComponent } from './components/pwdchange/pwdchange.component';
import { ActivateComponent } from './components/activate/activate.component';
import { LinkComponent } from './components/link/link.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FeedComponent } from './components/feed/feed.component';
import { PostComponent } from './components/post/post.component';
import { NewpostComponent } from './components/newpost/newpost.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProfilComponent } from './components/profil/profil.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { RequestpwdComponent } from './components/requestpwd/requestpwd.component';

const appRoutes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent},
  {path: 'admin', component: AdminComponent, canActivate: [AuthGuard]},
  {path: 'logout', component: LogoutComponent},
  {path: 'feed', component: FeedComponent, canActivate: [AuthGuard]},
  {path: 'pwdchange', component: PwdchangeComponent, canActivate: [AuthGuard]},
  {path: 'activate', component: ActivateComponent},
  {path: 'link', component: LinkComponent},
  {path: 'post', component: PostComponent},
  {path: 'newpost', component: NewpostComponent, canActivate: [AuthGuard]},
  {path: 'profil', component: ProfilComponent, canActivate: [AuthGuard]},
  {path: 'requestpwd/:reset_code', component: RequestpwdComponent},
  {path: 'requestpwd', component: RequestpwdComponent},
  {path: '**', component: NotfoundComponent}

];

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    LogoutComponent,
    PwdchangeComponent,
    ActivateComponent,
    LinkComponent,
    NavbarComponent,
    FeedComponent,
    PostComponent,
    NewpostComponent,
    ProfilComponent,
    NotfoundComponent,
    RequestpwdComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    AppRoutingModule,
    FormsModule,
    NgxSpinnerModule,
    HttpModule,
    ScrollEventModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ["api.elite21.fr"]  // TODO:  Modification du domaine de l'API | prod : api.elite21.fr | dev api.clelite.24jb.cc:8080

      }
    }),
  ],
  providers: [AuthentificationSystemService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
