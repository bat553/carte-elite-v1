import { TestBed } from '@angular/core/testing';

import { AuthentificationSystemService } from './authentification-system.service';

describe('AuthentificationSystemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthentificationSystemService = TestBed.get(AuthentificationSystemService);
    expect(service).toBeTruthy();
  });
});
