import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

interface Auth {
  msg: string;
  token: string;
  err_code: number;
  success: boolean;
}

interface User_infos {
  points_elite:  number,
  CAEL_id: number,
  username: string,
  card_type: string,
  departement: string,
  exp_date: number,
  success?: boolean,
  lastname: string,
  surname: string,
  admin: boolean,
  avatar_link?: any
}

@Injectable({
  providedIn: 'root'
})
export class AuthentificationSystemService {
  // variable declaration
  api_url: string  = "https://api.elite21.fr/api/v3/"; // TODO:  Modification du domaine de l'API | prod : httpS://api.elite21.fr/api/v3/ | dev api.clelite.24jb.cc:8080

  code: string;
  private loggedInStatus: boolean = false;

  error : ErrorDash =  null;
  logoutCode : boolean;
  user_info: User_infos;

  constructor(private http: HttpClient, private router: Router) {
    this.user_info = {
      username: "{XXXXXX}",
      lastname: "{XXXXXX}",
      surname: "{XXXXXX}",
      card_type: "{XXXXXX}",
      departement: "{XXXXXX}",
      exp_date: 9999999999,
      points_elite: 0,
      CAEL_id: 99999999,
      avatar_link: false,
      admin: false
    }
  }

  // Login point

  logout() {
    this.logoutCode = true;
    this.router.navigate(['logout'])
  }

  login() {
    this.router.navigate(['login'])
  }

  feed(){
    this.router.navigate(['feed'])
  }

  notFound(){
    this.router.navigate(['notfound'])
  }

  getQuote(){
    return this.http.get(this.api_url + "quote")
  }

  getUserList(){
    return this.http.get(this.api_url + "admin/users/list")
  }

  addQuote(quote, author, date){
    return this.http.post(this.api_url + 'admin/add_quote', {
      quote,
      author,
      date
    })
  }

  requestReset(email){
    return this.http.post(this.api_url + "reset", {
      email
    })
  }

  resetPwd(email, c_password, password, reset_code){
    return this.http.post(this.api_url + "user/reset", {
      email,
      c_password,
      password,
      reset_code
    })
  }

  updateCard(CAEL_id, revoked=null, CAEL_code=null, assign=null){
    return this.http.post(this.api_url + 'admin/update/card', {
        CAEL_id,
        revoked,
        CAEL_code,
        assign
    })
  }

  getPosts(page: number = 1){
    return this.http.get<Auth>(this.api_url + "posts/" + page)
  }

  adminCheck(){
/*    if (!this.user_info.admin || this.user_info.admin !== true){
      this.notFound();
    } else {
      this.error = {
        errorContent: "Bienvenue !",
        errorSeverity: "success"
      }
    } TODO : Corriger */
  }


  checkToken() {
    if (localStorage.getItem('access_token')){
      this.http.post(this.api_url+ 'token/check', {})
        .subscribe(res =>{
            if (res['success']){
              return true
          }
          else {
            this.error = {
              errorContent: res['msg'],
              errorLogout: true,
              errorSeverity: res['severity']
            }
            this.logout();
            return false
          }
        },
        error => {
          this.error = {
            errorContent: error['error']['msg'],
            errorLogout: true,
            errorSeverity: error['error']['severity']
          }
          this.logout();
          return false
        })
    } else {
      this.logout();
      return false
    }
  }

  authenticate(username, password) {
      return this.http.post<Auth>(this.api_url+ 'auth', {
        CAEL_id: username,
        username: username,
        password: password
      });
  }

  getUserData() {
    return this.http.get(this.api_url+ 'user')
      .subscribe(reply => {
        if (reply['success']) {
          this.user_info = {
            points_elite: reply['points_elite'],
            CAEL_id: reply['CAEL_id'],
            username: reply['username'],
            card_type: reply['card_type'],
            departement: reply['departement'],
            exp_date: reply['exp_date'],
            lastname: reply['lastname'],
            surname: reply['surname'],
            admin: reply['admin'],
            avatar_link: reply['avatar_link']
          }
        }
      }, error1 => {
        this.error = {
          errorContent: "User infos fetching error",
          errorSeverity: "danger"
        }
      })
  }
  getUser(username){
    return this.http.get(this.api_url + 'user/get_user/' + username)
  }

  signup(password, username, firstname="",lastname="", email=""){
      return this.http.put<Auth>(this.api_url+ 'admin/signup', {
        CAEL_username: username,
        CAEL_pwd: password,
        CAEL_surname: firstname,
        CAEL_lastname: lastname,
        CAEL_email: email
      })
  }
  changePassword(old_pwd, new_pwd){
    return this.http.post<Auth>(this.api_url+ 'user/changepassword', {
      old_pwd,
      new_pwd
    })
  }

  getPublicStats(){
    return this.http.get(this.api_url+ 'public/stats')
  }

  getCommit(){
    return this.http.get(this.api_url+ 'dev/commit')
  }

  postActivate(CAEL_id, password, username, email, lastname="", surname="", CAEL_code, birthday, gender, confirm_password){
    return this.http.post(this.api_url+ 'activate', {
      CAEL_id,
      username,
      email,
      password,
      lastname,
      surname,
      CAEL_code,
      birthday,
      gender,
      confirm_password
    })
  }


  addCard(CAEL_id){
    return this.http.put<Auth>(this.api_url+ 'admin/add_card', {
      CAEL_id
    })
  }
  getCardAdmin(CAEL_id){
    return this.http.post<Auth>(this.api_url+ 'admin/get_card', {
      CAEL_id
    })
  }

  getCards(user_id="null", revoked="null", assign="null", dep="null", type="null", page=""){
    return this.http.get<Auth>(this.api_url + 'admin/cards/' + user_id + '/' + revoked + '/' + assign + '/' + dep + '/' + type + '/' + page)
  }

  resetError(){
    if (this.error && (this.error.errorLogout == true || !this.error.errorLogout)) {
      // Si l'erreur ne vient pas du logout
      this.error = null;
      return true
    } else {
      return true
    }
  }

  linkCard(username, password, CAEL_id, CAEL_code){
    return this.http.post(this.api_url+ 'link', {
      CAEL_id,
      username,
      password,
      CAEL_code
    })
  }

  setVote(post_id, vote){
    return this.http.post<Auth>(this.api_url + "user/vote",  {
      post_id,
      vote
    })
  }

  getPost(post_id){
    return this.http.get<Auth>(this.api_url + 'post/' + post_id)
  }

  getCard(card_id){
    return this.http.get<Auth>(this.api_url + 'card/' + card_id)
  }

  epochToDate(epoch){
    var utcSeconds = epoch;
    var d = new Date(utcSeconds*1000);
    return(
      d.getFullYear() + "-" +
      ("00" + (d.getMonth() + 1)).slice(-2) + "-" +
      ("00" + d.getDate()).slice(-2) + " " +
      ("00" + d.getHours()).slice(-2) + ":" +
      ("00" + d.getMinutes()).slice(-2) + ":" +
      ("00" + d.getSeconds()).slice(-2)
    );
  }

}

export interface Infos {
  CAEL_id: number;
  username: string;
  credit: number;
  point: number;
  surname?: string;
  name?: string;
  admin?: Boolean;
  departement?: String;
  card_type?: String;
}

export interface ErrorDash {
  errorName?: string;
  errorContent: string;
  errorSeverity: string;
  errorCode?: number;
  errorAlternate?: string;
  errorUi?: number;
  errorBackend?: boolean;
  errorLogout?: boolean
}

export interface Post {
  content : {
    title: String,
    desc: String,
    pic_link
  },
  created_at: Number,
  point_elite: Number,
  created_by: String
}
