import { Component, OnInit } from '@angular/core';
import {AuthentificationSystemService} from "../../services/authentification-system.service";
import {angularCoreEnv} from "@angular/core/src/render3/jit/environment";
import {faPaperPlane, faArrowCircleLeft} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-newpost',
  templateUrl: './newpost.component.html',
  styleUrls: ['./newpost.component.css']
})
export class NewpostComponent implements OnInit {

  //Icons
  faPaperPlane = faPaperPlane
  faArrowCircleLeft = faArrowCircleLeft

  constructor(public Auth: AuthentificationSystemService) { }

  ngOnInit() {
    this.Auth.resetError()
  }

  addPost(event){
    event.preventDefault()
    const target = event.target
    const title = target.querySelector('#title').value;
    const desc = target.querySelector('#desc').value;
    const image = target.querySelector('#image');
    const progress = document.getElementById('progress')
    const uploading = document.getElementById('uploading')
    const pic_url = target.querySelector('#pic_url').value
    this.Auth.resetError()

    var token = localStorage.getItem('access_token')

    progress.style.width = 0 + "%";
    progress.classList.remove('bg-success')
    uploading.classList.remove('d-none')


    var xhr = new XMLHttpRequest();
    xhr.open('POST', this.Auth.api_url + 'user/post', true);
    xhr.setRequestHeader("Authorization", "Bearer " + token)

    xhr.upload.addEventListener('progress', function (e) {
      progress.style.width = e.loaded / e.total * 100 + "%";

    }, false);


    xhr.addEventListener('load', function (e) {
      progress.classList.add("bg-success")
    }, false);

    xhr.addEventListener('error', function (e) {
      progress.classList.add("bg-danger")
      uploading.classList.add('alert-danger')
      uploading.innerText = "Upload failed"
    }, false)
    xhr.addEventListener('timeout', function (e) {
      progress.classList.add("bg-waring")
    }, false)

    xhr.onreadystatechange = function (e) {
      if (xhr.readyState === 4) { // Reponse
          if (xhr.status !== 201){
            var resp = JSON.parse(xhr.responseText)
            progress.classList.add("bg-danger")
            uploading.classList.remove('alert-info')
            uploading.classList.add('alert-danger')
            uploading.classList.remove('d-none')
            if (resp ['err_code'] == 47) {
              // Si c'est l'anti flood
              uploading.innerText = "Upload failed. " + resp['msg'] + " Temps restant : " + Math.round((resp["temps_restant"]) / -1) + " secondes"
            } else {
              uploading.innerText = "Upload failed. " + resp['msg']
            }
          } else {
            progress.classList.remove("bg-danger")
            uploading.classList.remove("alert-danger")
            progress.classList.add("bg-success")
            uploading.classList.add('alert-success')
            uploading.classList.remove('d-none')
            uploading.innerText = "Bien reçu, merci pour votre contribution! Redirection vers votre fil d'élite."
            setTimeout(function () {
              document.location.href="/feed"
            }, 2000)
          }
      }
    }
    if (xhr.status !== 200){
      this.Auth.checkToken();
    }
    if (title && (image.files[0] || desc || pic_url)) {
      var form = new FormData();
      form.append('image', image.files[0]);
      form.append('title', title);
      form.append('desc', desc);
      form.append('pic_url', pic_url);
      xhr.send(form);
    } else {
      this.Auth.error = {
        errorContent: "Merci de remplire le titre et au moins la description ou l'image",
        errorSeverity: "warning",
        errorCode: 3,
        errorUi: 1
      }
      }

  }


}
