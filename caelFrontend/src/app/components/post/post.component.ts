import {Component, Input, OnInit} from '@angular/core';
import {AuthentificationSystemService} from "../../services/authentification-system.service";
import {faArrowUp, faArrowDown} from "@fortawesome/free-solid-svg-icons";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})

export class PostComponent implements OnInit {

  @Input() post_id: string

  // Icons
  faArrowUp = faArrowUp
  faArrowDown = faArrowDown

  post: PostObj
  user: object
  vote_resp: object
  upvote: string
  downvote: string

  constructor(public Auth: AuthentificationSystemService) {
    this.post = {
      post:{
        points_elite: 0,
        created_by: "Loading.",
        created_at: 0,
        _id: "",
        content: {
          title: "Loading...",
          desc: "Loading.."
        }
      }, user: {
        points_elite:0,
        username: "Loading",
        avatar_link: null
      }
    }
    this.user = {
      username: "Loading..."
    }
  }

  ngOnInit() {
      this.getPost(this.post_id)
  }



  vote(post_id, vote){

      this.Auth.resetError();
      this.Auth.setVote(post_id, vote)
        .subscribe(reply => {
          if (reply['success']) {
            this.vote_resp = reply['doc'];
            if (reply['action'] == "cancel") {
              // Si on annule le vote précedent
              document.getElementById('upvote-' + post_id).style.color = "#000"
              document.getElementById('downvote-' + post_id).style.color = "#000"
            } else if (vote == "up") {
              // Si c'est un upvote
              document.getElementById('downvote-' + post_id).style.color = "#000"
              document.getElementById('upvote-' + post_id).style.color = "#fff"
            } else if (vote == "down") {
              // Si c'est un downvote
              document.getElementById('upvote-' + post_id).style.color = "#000"
              document.getElementById('downvote-' + post_id).style.color = "#fff"
            }
            this.ngOnInit()
            this.Auth.checkToken()
          }
        }, error1 => {
          this.Auth.error = {
            errorContent: error1['error']['msg'],
            errorSeverity: error1['error']['severity'],
            errorUi: post_id
          }
        })

  }

  getPostUser(username){
    this.Auth.getUser(username)
      .subscribe(reply =>{
        this.post['user_infos'] = reply['user_infos']
      })
  }

  getPost(post_id){
    this.Auth.getPost(this.post_id)
      .subscribe(reply => {
        this.post.post = reply['post']
        var post_id = reply['post']['_id']
        this.post.user = reply['user']
        try {
          var vote = reply['vote']['voted_posts'][0]['vote']
        } catch {
          // Pas de vote
          this.upvote = this.downvote = "unset"
          var vote = null
        }
        if (vote == true){
          // Si c'est un upvote
          this.upvote = "set"
          this.downvote = "unset"
        } else if (vote == false) {
          // Si c'est un downvote
          this.upvote = "unset"
          this.downvote = "set"
        }
      }, error1 => {
        this.Auth.checkToken()
      })
  }
}

interface PostObj {
  success?: boolean,
  post: {
    _id: string,
    points_elite: number,
    created_by: string,
    content: {
      title: string,
      desc?: string,
      pic_link?: string,
      deletehash?: string
    },
    created_at: number
  }
  vote?: any,
  user: {
    points_elite: number,
    _id?: string,
    username: string,
    gender?: string,
    bithday?: string,
    avatar_link:string
  }
}
