import { Component, OnInit } from '@angular/core';
import {AuthentificationSystemService} from "../../services/authentification-system.service";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  message: string = 'Loading...';
  res: Object;
  card_infos: Object;
  cards_list : object;
  card_list_page: object;
  users_list: object;

  constructor(public Auth: AuthentificationSystemService) {

  }

  addCard(event){
    event.preventDefault();
    const target = event.target;
    const CAEL_id = target.querySelector('#newCAEL_id').value;

    this.Auth.checkToken();
    this.Auth.resetError()

    if (!CAEL_id){
      this.Auth.error = {
        errorSeverity: "warning",
        errorContent: "Please fill CAEL_id.",
        errorUi: 5
      }
    } else {
        this.Auth.addCard(CAEL_id)
          .subscribe(reply => {
            this.res = reply;
            if (this.res["success"]){
              this.Auth.error = {
                errorSeverity: "success",
                errorContent: this.res["msg"] + " type : " + this.res['type'] + " departement : " + this.res['departement'] + " code : " + this.res['CAEL_code'],
                errorUi: 5
              }
            } else {
              this.Auth.error = {
                errorSeverity: this.res["severity"],
                errorContent: this.res["msg"],
                errorUi: 5
              }
            }
          }, error1 => {
            this.res = error1["error"];
            this.Auth.error = {
              errorSeverity: this.res["severity"],
              errorContent: this.res["msg"],
              errorUi: 5
            }
          })
    }

  }

  getUsers(event){
    event.preventDefault()
    const target = event.target;
    this.Auth.resetError()

    this.Auth.getUserList()
      .subscribe(reply => {
        this.users_list = reply['users']['docs']

      }, error1 => {
        this.Auth.error = {
          errorSeverity: error1['error']['severity'],
          errorUi: 3,
          errorLogout: false,
          errorContent: error1['error']['msg']
        }

      })


  }

  getCard(event){
    event.preventDefault();
    const target = event.target;
    const CAEL_id = target.querySelector('#getCAEL_id').value;
    this.Auth.resetError()

    if (!CAEL_id){
      this.Auth.error = {
        errorSeverity: "warning",
        errorContent: "Please fill CAEL_id.",
        errorUi: 6
      }
    } else {
      this.Auth.checkToken();
      this.Auth.getCardAdmin(CAEL_id)
        .subscribe(reply=> {
            if (reply["success"]){
              this.card_infos = reply["card_infos"]
              this.Auth.error = {
                errorUi: 6,
                errorContent: reply["msg"],
                errorSeverity: "info"
              }
            } else {
              this.Auth.error = {
                errorSeverity: reply["severity"],
                errorContent: reply["msg"],
                errorUi: 6
            }
          }
        }, error1 => {
          this.res = error1["error"];
          this.Auth.error = {
            errorSeverity: this.res["severity"],
            errorContent: this.res["msg"],
            errorUi: 6
          }
        })
    }

  }

  updateCard(event){
      event.preventDefault()
      var target = event.target
      var CAEL_id = target.querySelector('#m_CAEL_id').value
      var revoked = target.querySelector('#m_revoked').value
      var assign = target.querySelector('#m_assign').value
      var CAEL_code = target.querySelector('#m_CAEL_code').value

      this.Auth.resetError()
      this.Auth.checkToken()
    if (CAEL_id) {
      this.Auth.updateCard(CAEL_id, revoked, CAEL_code, assign)
        .subscribe(reply => {
          this.Auth.error = {
            errorContent: reply['msg'],
            errorSeverity: reply['severity'],
            errorUi: 1
          }
        }, error1 => {
          this.Auth.error = {
            errorContent: error1['error']['msg'],
            errorSeverity: error1['error']['severity'],
            errorUi: 1
          }
        })
    } else{
      this.Auth.error = {
        errorUi:1,
        errorContent: "Merci de saisir le no de carte élite",
        errorSeverity: "warning"
      }
    }
  }

  getCards(event){
    event.preventDefault()
    var target = event.target
    var revoked = target.querySelector('#revoked').value
    var assign = target.querySelector('#assign').value
    var user_id = target.querySelector('#user_id').value
    var type = target.querySelector('#type').value
    var dep = target.querySelector('#dep').value
    this.Auth.resetError()
    this.Auth.checkToken()

    this.Auth.getCards(user_id || "null", revoked, assign, dep|| "null", type|| "null")
      .subscribe(reply => {
        this.card_list_page = reply['cards']['pages']
        this.cards_list = reply['cards']['docs']
      })


  }

  addQuote(event){
    event.preventDefault()
    var target = event.target
    var quote = target.querySelector('#quote').value
    var author = target.querySelector('#author').value
    var date = target.querySelector('#date').value

    this.Auth.resetError()
    this.Auth.checkToken()

    if (quote && author && date){
      this.Auth.addQuote(quote, author, date)
        .subscribe(reply => {
          if(reply["success"]){
            this.Auth.error = {
              errorSeverity: "success",
              errorUi: 2,
              errorContent: reply['msg']
            }
          }else {
            this.Auth.error = {
              errorSeverity: reply['severity'],
              errorUi: 2,
              errorContent: reply['msg']
            }
          }
        }, error1 => {
          this.Auth.error = {
            errorContent: error1['error']['msg'],
            errorUi: 2,
            errorSeverity: error1['error']['severity']
          }
        })
    } else {
      this.Auth.error = {
        errorContent: "Merci de remplir tous les champs",
        errorUi: 2,
        errorSeverity: "warning"
      }
    }


  }

  ngOnInit() {
      this.Auth.adminCheck()
  }

}

