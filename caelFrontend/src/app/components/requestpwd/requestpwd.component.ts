import {Component, Input, OnInit} from '@angular/core';
import {AuthentificationSystemService} from "../../services/authentification-system.service";
import {AppComponent} from "../../app.component";
import {faKey, faArrowAltCircleLeft} from "@fortawesome/free-solid-svg-icons";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-requestpwd',
  templateUrl: './requestpwd.component.html',
  styleUrls: ['./requestpwd.component.css']
})
export class RequestpwdComponent implements OnInit {
  // ICON
  faKey = faKey
  faArrowAltCircleLeft = faArrowAltCircleLeft


  public reset_code : string

  constructor(public Auth: AuthentificationSystemService, public appDetails : AppComponent, public route: ActivatedRoute) { }
  ngOnInit() {
    this.reset_code = this.route.snapshot.paramMap.get('reset_code')
    this.Auth.resetError()
  }

  requestpwd(event){
    event.preventDefault();
    const target = event.target;
    const email = target.querySelector('#email').value;
    this.Auth.resetError()

    this.Auth.requestReset(email)
      .subscribe(reply=>{
        this.Auth.error = {
          errorContent : reply['msg'],
          errorLogout: true,
          errorSeverity: reply['severity']
        }
      }, error1 => {
        this.Auth.error ={
          errorSeverity : error1['error']['severity'],
          errorLogout: false,
          errorContent: error1['error']['msg']
        }
      })
  }
  resetpwd(event){
    event.preventDefault();
    const target = event.target;
    const email = target.querySelector('#email').value;
    const reset_code = target.querySelector('#reset_code').value;
    const password = target.querySelector('#password').value;
    const c_password = target.querySelector('#c_password').value;
    this.Auth.resetError()


    if (!email || !reset_code || !password || !c_password){
      this.Auth.error = {
        errorContent: "Merci de remplir tous les champs",
        errorSeverity: "warning"
      }
    } else if (c_password !== password){
      this.Auth.error = {
        errorContent: "Mots de passe non correspondants",
        errorSeverity: "warning"
      }
    } else {
      this.Auth.resetPwd(email, c_password, password, reset_code)
        .subscribe(reply=>{
          this.Auth.error = {
            errorContent : reply['msg'],
            errorLogout: true,
            errorSeverity: reply['severity']
          },
          this.Auth.login()
        }, error1 => {
          this.Auth.error ={
            errorSeverity : error1['error']['severity'],
            errorLogout: false,
            errorContent: error1['error']['msg']
          }
        })
    }
  }

}
