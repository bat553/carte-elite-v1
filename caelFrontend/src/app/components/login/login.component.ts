import { Component, OnInit } from '@angular/core';
import { AuthentificationSystemService, ErrorDash } from '../../services/authentification-system.service';
import {Router} from '@angular/router';
import {HttpClient} from "@angular/common/http";
import {AppComponent} from "../../app.component";
import {faLock, faArrowCircleRight, faPlusCircle, faKey} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {

  resp: Object;
  error: ErrorDash;

  // Icons
  faLock = faLock
  faArrowCircleRight = faArrowCircleRight
  faPlusCircle = faPlusCircle
  faKey = faKey


  constructor(public Auth: AuthentificationSystemService, private router: Router, private http: HttpClient, public appDetails : AppComponent) { }
  ngOnInit() {
    if (this.Auth.error && !this.Auth.error.errorLogout) {
      // Si c'est une erreur qui n'est pas un logout, reset
      this.Auth.resetError();
    }else {
      this.redirectFeed();
    }
  }

  redirectFeed(){
    if (localStorage.getItem('access_token')){
      this.http.post(this.Auth.api_url+'token/check', {})
        .subscribe(res =>{
            if (res['success']){
              this.router.navigate(['feed'])
            }
            else {
              return false;
            }
          },
          error => {return false;})
    } else {
      return false;
    }
  }


  loginUser(event) {
    event.preventDefault();
    const target = event.target;
    const username = target.querySelector('#username').value;
    const password = target.querySelector('#password').value;
    this.Auth.error = {
      errorSeverity: "info",
      errorContent: "Authentification en cours..."
    };

    if (!username || !password){
      this.Auth.error = {
        errorSeverity : "warning",
        errorContent: "Missing fields",
        errorCode: 3,
        errorAlternate: "oops, account not found"
      }
    }
    else {
      this.Auth.authenticate(username, password)
        .subscribe(data => {
            this.resp = data;
            if (this.resp['success']) {
              this.Auth.code = 'Login success!';
              localStorage.setItem('access_token', this.resp['token']);
              this.router.navigate(['feed']);
            }
          },
          error1 => {
            this.resp = error1['error'];
            this.Auth.code = 'Login failed !';
            if (error1['status'] == 504) {
              this.Auth.error = {
                errorContent: "Error server... Please retry later",
                errorCode: 504,
                errorSeverity: "danger"
              }
            }
            else {
              this.Auth.error = {
                errorContent: this.resp['msg'],
                errorCode: this.resp['err_code'],
                errorSeverity: this.resp['severity']
              }
            }
          });

    }
  }

}
