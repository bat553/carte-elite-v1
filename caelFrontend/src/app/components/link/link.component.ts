import { Component, OnInit } from '@angular/core';
import {AppComponent} from "../../app.component";
import {AuthentificationSystemService} from "../../services/authentification-system.service";
import {faCheckCircle, faArrowCircleLeft} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.css']
})
export class LinkComponent implements OnInit {

  // Icons
  faCheckCircle = faCheckCircle
  faArrowCircleLeft = faArrowCircleLeft

  constructor(public appDetails: AppComponent, public Auth: AuthentificationSystemService) {}

  ngOnInit() {
  }

  resp :Object;

  linkCard(event){
    event.preventDefault();
    const target = event.target;
    const username = target.querySelector('#username').value;
    const password = target.querySelector('#password').value;
    const CAEL_id = target.querySelector('#CAEL_id').value;
    const CAEL_code = target.querySelector('#CAEL_code').value;

    if (!username || !password || !CAEL_id || !CAEL_code) {
      this.Auth.error = {
        errorSeverity: "danger",
        errorCode: 0,
        errorName: "false",
        errorContent: "Merci de remplir tous les champs."
      }
    } else {
      this.Auth.linkCard(username, password, CAEL_id, CAEL_code)
        .subscribe(reply => {
          if (reply['success']) {
            this.Auth.error = {
              errorContent: reply['msg'],
              errorSeverity: reply['severity'],
              errorCode: reply['err_code']
            },
              this.Auth.logout()
          }
        }, error1 => {
          this.resp = error1['error'];
          if (this.resp["err_code"] == 36) {
            // Si l'utilisateur à deja une carte active
            this.Auth.error = {
              errorContent: "Vous avez deja une carte valide. Pour effectuer un changement, merci de vous rendre sur votre espace personnel.",
              errorSeverity: this.resp['severity'],
              errorCode: this.resp['err_code']
            }
          } else {
            this.Auth.error = {
              errorContent: this.resp['msg'],
              errorSeverity: this.resp['severity'],
              errorCode: this.resp['err_code']
            }
          }
        })
    }
  }

}
