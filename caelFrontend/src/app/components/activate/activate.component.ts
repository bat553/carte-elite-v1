import { Component, OnInit } from '@angular/core';
import {AuthentificationSystemService} from "../../services/authentification-system.service";
import {AppComponent} from "../../app.component";
import {faCheckCircle, faArrowCircleLeft} from "@fortawesome/free-solid-svg-icons";


@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.css']
})
export class ActivateComponent implements OnInit {

  // Icons
  faCheckCircle = faCheckCircle
  faArrowCircleLeft = faArrowCircleLeft

  constructor(public Auth: AuthentificationSystemService, public appDetails: AppComponent) { }
  resp = Object;
  ngOnInit() {
    this.Auth.resetError();
  }
  activateCard(event) {
    event.preventDefault();
    const target = event.target;

    const lastname = target.querySelector('#lastname').value;
    const surname = target.querySelector('#surname').value;
    const gender = target.querySelector('#gender').value;
    const birthday = target.querySelector('#birthday').value;

    const username = target.querySelector('#username').value;
    const email = target.querySelector('#email').value;
    const password = target.querySelector('#password').value;
    const confirm_password = target.querySelector('#confirm_password').value;
    const CAEL_id = target.querySelector('#CAEL_id').value;
    const CAEL_code = target.querySelector('#CAEL_code').value;




    this.Auth.error = null;

    if (!CAEL_id || !username || !email || !password ){
      this.Auth.error = {
        errorContent: "Please fill all fields",
        errorSeverity: "danger"
      }
    } else if (CAEL_id.length != 8 || username.length >= 11 || username.length < 5 || lastname.length > 15 || surname.length > 15 || password.length > 42 || CAEL_code.length > 8){
      this.Auth.error = {
        errorName: "Please fix your input.",
        errorContent: "Card no length = 8, username entre 5 et 11 caractères, lastname et surname moins de 15 caratères chaqun, password de moins de 42 caractères",
        errorSeverity: "warning"
      }
    }
    else {
      this.Auth.postActivate(CAEL_id, password, username, email, lastname, surname, CAEL_code, birthday, gender, confirm_password)
        .subscribe(reply => {
          if (reply['success']) {
            this.Auth.error = {
              errorContent: reply['msg'],
              errorSeverity: reply['severity'],
              errorCode: reply['err_code'],
              errorLogout: true
            },
              this.Auth.logout();
          }
        }, error1 => {
          this.resp = error1['error'];
          this.Auth.error = {
              errorContent: this.resp['msg'],
              errorSeverity: this.resp['severity'],
              errorCode: this.resp['err_code']
          }
        })
    }


  }
}
