import { Component, OnInit, ViewChild  } from '@angular/core';
import {AuthentificationSystemService} from "../../services/authentification-system.service";
import {NavbarComponent} from "../navbar/navbar.component";
import {faSyncAlt, faTools, faPlusCircle} from "@fortawesome/free-solid-svg-icons";
import {ScrollEvent} from "ngx-scroll-event";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})


export class FeedComponent implements OnInit {

  constructor(public Auth: AuthentificationSystemService, private spinner: NgxSpinnerService) {
    window.scrollTo(0, 0)
    this.quote = {
      quote: "Les chargements, c'est long",
      author: "Platon",
      date: "3242 avant J.C."
    }
  }

  // Icons
  faSyncAlt = faSyncAlt
  faTools = faTools
  faPlusCircle = faPlusCircle

  quote: Quote
  post: object;
  vote_resp: object;
  posts_list = []
  post_infos : Post;
  private current_page: number = 1;
  public pages: number;
  public end: boolean = false;

  public handleScroll(event: ScrollEvent) {
    function sleep(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }
    }
    if (event.isReachingBottom) {
      this.end = false
      this.onScrollDown()
    }
    sleep(100) // Pause de 100 milli seconde (actualisation de la position)
  }

  ngOnInit() {
    // Spinner
    this.spinner.show()
    setTimeout(() => {
      this.spinner.hide()
    }, 1000)


    this.Auth.resetError()
    this.getPosts();
    this.getQuote()
  }

  public getQuote(){
    this.Auth.getQuote()
      .subscribe(reply =>{
        this.quote = {
          quote: reply['quote'],
          author: reply['author'],
          date: reply['date']
        }
      }, error1 => {
        this.quote = {
          quote: "Au fil de la connerie...",
          author: "les ohms au cube",
          date: "2019"
        }
      })
  }

  public onScrollDown(): void {
    if (this.current_page < this.pages) {
      this.current_page++;
      this.getPosts(this.current_page)
    } else{
      this.end = true
    }
  }


 private getPosts(page: number = 1){
    this.Auth.getPosts(page)
      .subscribe(reply => {
        if (reply['success']){
          this.posts_list.push(reply['posts']['docs'])
          this.pages = reply['posts']['pages']
        }
      }, error1 => {
        this.posts_list = null
        this.Auth.error = {
          errorContent : "No content found.",
          errorSeverity: "info",
          errorCode: 1,
          errorUi: 2
        }
      });
  }

  getPost(post_id){
    this.Auth.getPost(post_id)
      .subscribe(reply => {
          this.post_infos = {
            content : {
              title: reply['title'],
              pic_link: reply['pic_link'],
              desc: reply['desc']
            },
            created_at: reply['dreated_at'],
            created_by: reply['created_by'],
            _id: reply['_id'],
            points_elite: reply['points_elite']
        }
      }, error1 => {
        return {
          content: {
            title: error1["msg"]
          }
        }
      })
  }

  updatePoint(point, vote){
    if (vote == "down"){
      point = point -1
    } else if (vote == "up"){
      point = point + 1
    } else if (vote == "undo"){
      point = point
    }
  }

}


export interface Post {
  content: {
    title: String,
    desc?: String,
    pic_link?: String
  },
  created_at : Number,
  points_elite : Number,
  _id: String,
  created_by: String
}

interface Quote {
  quote: string,
  author: string,
  date: string
}
