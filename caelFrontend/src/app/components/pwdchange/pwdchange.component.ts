import { Component, OnInit } from '@angular/core';
import {AuthentificationSystemService, ErrorDash} from "../../services/authentification-system.service";
import {AppComponent} from "../../app.component";
import {faKey} from "@fortawesome/free-solid-svg-icons/faKey";
import {faArrowAltCircleLeft} from "@fortawesome/free-solid-svg-icons/faArrowAltCircleLeft";

@Component({
  selector: 'app-pwdchange',
  templateUrl: './pwdchange.component.html',
  styleUrls: ['./pwdchange.component.css']
})
export class PwdchangeComponent implements OnInit {

  // ICON
  faKey = faKey
  faArrowAltCircleLeft = faArrowAltCircleLeft

  constructor(public Auth: AuthentificationSystemService, public  appDetails: AppComponent) { }

  ngOnInit() {
    this.Auth.resetError();
  }
  error: ErrorDash;
  resp = Object;

  pwdChange(event){
    event.preventDefault();
    const target = event.target;
    const old_pwd = target.querySelector('#old_pwd').value;
    const new_pwd = target.querySelector('#new_pwd').value;
    const new_pwd_conf = target.querySelector('#new_pwd_conf').value;
    this.Auth.error = null;

    if (!old_pwd || !new_pwd || !new_pwd_conf){
      this.Auth.error = {
        errorContent: "Please fill all fields",
        errorSeverity: "danger"
      }
    }
    else if (new_pwd !== new_pwd_conf){
      this.Auth.error = {
        errorContent: "Passwords do not match",
        errorSeverity: "warning"
      }
    }
    else {
      this.Auth.checkToken();
      this.Auth.changePassword(old_pwd, new_pwd)
          .subscribe(
            reply => {
              if (reply.success){
                this.Auth.error = {
                  errorSeverity:"info",
                  errorContent: "Password changed ! ",
                  errorCode: 18,
                  errorLogout: true
                };
                this.Auth.logout();
              }
            },
            error1 => {
              this.resp = error1['error'];
              this.Auth.error = {
                errorContent: this.resp['msg'],
                errorCode: this.resp['err_code'],
                errorSeverity: "danger"
              }
            }
          )
    }
  }


}
