import { Component, OnInit } from '@angular/core';
import {AuthentificationSystemService} from "../../services/authentification-system.service";
import {faBars} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {

  // ICONS
  faBars = faBars


  constructor(public Auth: AuthentificationSystemService) { }


  ngOnInit() {
    this.Auth.getUserData();
    this.Auth.checkToken()
  }

}
