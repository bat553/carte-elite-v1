import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthentificationSystemService} from "../../services/authentification-system.service";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router, private Auth: AuthentificationSystemService) { }

  ngOnInit() {
      if (!this.Auth.logoutCode){
        // Si c'est un logout direct (bouton logout)
        this.Auth.error = {
          errorCode: 0,
          errorContent: "Logout sucessfull",
          errorSeverity: "success",
          errorLogout: true
        }
      }
      localStorage.clear()
      this.router.navigate(['login'])
  }

}
