import { Component, OnInit } from '@angular/core';
import {AuthentificationSystemService} from "../../services/authentification-system.service";
import {faPaperPlane, faArrowCircleLeft} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  //ICONS
  faArrowCircleLeft = faArrowCircleLeft
  faPaperPlane = faPaperPlane

  constructor(public Auth: AuthentificationSystemService) {}

  ngOnInit() {
  }

  update(event){
    event.preventDefault()
    const target = event.target;
    const avatar = target.querySelector('#avatar')
    const progress = document.getElementById('progress')
    const uploading = document.getElementById('uploading')
    this.Auth.checkToken()
    this.Auth.resetError()

    var token = localStorage.getItem('access_token')

    var xhr = new XMLHttpRequest();
    xhr.open('POST', this.Auth.api_url + 'user/update', true);
    xhr.setRequestHeader("Authorization", "Bearer " + token)
    xhr.upload.addEventListener('progress', function (e) {
      progress.style.width = e.loaded / e.total * 100 + "%";
      uploading.classList.remove('d-none')
      uploading.classList.add('alert-info')
    }, false);

    xhr.onreadystatechange = function (e) {
      if (xhr.readyState === 4) { // Reponse
        if (xhr.status !== 200){
          progress.classList.add("bg-danger")
          uploading.classList.remove('alert-info')
          uploading.classList.add('alert-danger')
          uploading.classList.remove('d-none')
          uploading.innerText = "Upload failed. "+JSON.parse(xhr.responseText)['msg']
          console.log()
        } else {
          progress.classList.add("bg-success")
          uploading.classList.add('alert-success')
          uploading.classList.remove('d-none')
          uploading.classList.remove('alert-info')
          uploading.innerText = "Upload success!"
        }
      }
    }

    if (avatar.files[0]) {
      var form = new FormData();
      form.append('avatar', avatar.files[0]);
      xhr.send(form);
    } else {
      this.Auth.error = {
        errorContent: "Merci de sélectionner une image",
        errorSeverity: "warning",
        errorCode: 3,
        errorUi: 1
      }
    }
  }



}
