import { Component } from '@angular/core';
import {AuthentificationSystemService} from "./services/authentification-system.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor (private Auth: AuthentificationSystemService){}

  denv : ENV_COMMIT = {
    commit: "{{XXXXX}}",
    display: "d-none"
  }

  ngOnInit(){
    this.Auth.getCommit()
      .subscribe(reply =>{
          if (reply['success']){
            this.denv = {
              commit: reply["commit"],
              display: reply["display"]
            }
          }
      });
  }

  appName = "CAEL";
  version = "Beta-0.3-Dev";
  appNameLong = "La Carte Élite";
  pageTitle = "L'elite, le site";
  dashboardName = "Votre compte Élite."

  nav = {
    title: "C l'elite",
    logo: null,
    navItems: [
      {title: "Test1", link: "http://link.fr"},
      {title: "test2", link: "http://link2.fr"}
    ]
  }

}

interface ENV_COMMIT {
  commit: String,
  display: String
}
