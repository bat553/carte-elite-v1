#!/usr/bin/env bash
cd CAEL_API && npm install
cd ../caelFrontend && npm install && npm run build
cd .. && cp caelFrontendnginx /etc/nginx/sites-enabled/default
mkdir -p /var/log/CAEL/ /var/CAEL
mv * /var/CAEL/
cd /var/CAEL/
mongod &
mongoimport --host 127.0.0.1 --db CAEL_STOCK_OP --collection users --type json --file test_datas_mongo_users.json
mongoimport --host 127.0.0.1 --db CAEL_STOCK_OP --collection transactions_table --type json --file test_datas_mongo_transactions.json
mongoimport --host 127.0.0.1 --db CAEL_STOCK_OP --collection cards_table --type json --file test_datas_mongo_cards.json
nodejs CAEL_API/server.js &
pip3 install -r transaction_backend/requirements.txt
python3 transaction_backend/main.py &
nginx &
sleep 2
curl -I "http://127.0.0.1:8080/api/v1/public/stats"
curl -I "http://127.0.0.1/login"
cd CAEL_API
npm test test/tests-staging.spec.ts
echo "All done ! Prod deployment..."