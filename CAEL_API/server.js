require('dotenv').load();
var morgan = require('morgan');
var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var db = require("./app/config/db");
var passport = require('passport');
var cors = require('cors');
var jwt = require('passport-jwt');
var apiconfig = require('./app/config/api')

var app = express();
var port  = process.env.PORT || 8080;

// Request parameters
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// log to console
app.use(morgan(process.env.NODE_ENV || 'dev'));


// CORS


// Use the passport package (auth)
app.use(passport.initialize({
    session:false,
    secret: db.secret
}));

// Uploads files
app.use(cors({
    methods: "GET,POST,PUT",
    maxAge: 86400
}));

// connect to mongodb
mongoose.connect(db.url, { useNewUrlParser: true}, function (err) {
    if (err) return console.log(err);
    console.log("Api Version : " + apiconfig.api_version + " Environnement : " + process.env.NODE_ENV)

    if (process.env.NODE_ENV === "dev") {
        // Routes trafiquées pour le dev (pas d'authentification)
        require('./app/routes/admin/dev/dev_routes')(app);
    }


    require('./app/routes/public/public_routes')(app);
    require('./app/routes/user/user_routes')(app);
    require('./app/routes/admin/admin_routes')(app);
    require('./app/routes/cael_routes')(app);

    app.listen(port, function () {
        console.log("Server started on : " + port)
    });

});





