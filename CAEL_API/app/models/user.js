var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var functions = require('../functions')
var mongoosePaginate = require('mongoose-paginate');
var BCRYPT_SALT_ROUNDS = 10;

// Si problème avec "uniqueId" (E11000) : mongoshell$ db.collection.createIndex({username :1},{unique:true})

var UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true,
        index: true
    },
    password: {
        type: String,
        required: true
    },
    lastname: String,
    surname: String,
    email:{
        type: String,
        unique: true,
        required: true,
        index: true
    },
    admin:Boolean,
    registration_time: {
        type: Number,
        unique: true,
        index: true,
        required: true,
        default : function (){
            return functions.getEpoch()
        }
    },
    birthday : String,
    gender: String,
    voted_posts : [{
        post_id : {required: true, type: String},
        vote : {required: true, type: Boolean},
        vote_time: {
            required : true,
            unique: true,
            type: Number,
            default: function () {
                return functions.getEpoch()
            }
        }
    }],
    posts: [{
        post_id: {reqired: true, type: String},
        post_time: {
            required : true,
            unique: true,
            type: Number,
            default: function () {
                return functions.getEpoch()
            }
        }
    }],
    points_elite: {
        type: Number,
        default: 0
    },
    disable: {
        type: Boolean,
        required: true,
        default: false
    },
    avatar_link: String,
    deletehash: String,
    lastLogin: {
        type: Number,
        required: true,
        default: function () {
            return functions.getEpoch()
        }
    }
});


UserSchema.pre('save', function (next) {
    var user = this;
    if (this.password){
        bcrypt.genSalt(BCRYPT_SALT_ROUNDS, function(err, salt) {
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                // Store hash in your password DB.
                if (err) return next(err);
                user.password = hash;
            });
            next();
        });
    } else {
        return next();
    }
});


UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};
UserSchema.plugin(mongoosePaginate)

module.exports = mongoose.model('User', UserSchema, 'users');