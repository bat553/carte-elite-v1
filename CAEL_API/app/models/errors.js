// Error list

var Error = ({
    // Error code : eXX
    eXX: {
        msg: "",
        err_code: 0,
        severity: "",
        http_status: 200
    },
    e01: {
        success: false,
        msg: "You can'give yourself points.",
        err_code: 1,
        severity: "warning",
        http_status: 401
    },
    e02: {
        success: false,
        msg: "Unable to find user : ",
        err_code: 2,
        severity: "warning",
        http_status: 404,
        msg_default: "Unable to find user : "
    },
    e03: {
        success: false,
        msg: "Missing field",
        err_code: 3,
        severity: "danger",
        http_status: 400
    },
    e04: {
        success: false,
        msg: "User not found.",
        err_code: 4,
        severity: "danger",
        http_status: 404
    },
    e05: {
        success: false,
        msg: "Mauvais nom d'utilisateur ou mot de passe.",
        err_code: 5,
        severity: "danger",
        http_status: 401
    },
    e06: {
        success: false,
        msg: "Username or email already exist",
        err_code: 6,
        severity: "danger",
        http_status: 409
    },
    e07: {
        success: false,
        msg: "No client ip address recovered.",
        err_code: 7,
        severity: "danger",
        http_status: 500
    },
    e08: {
        success: false,
        msg: "Internal Error, retry in few minutes...",
        err_code: 8,
        severity: "danger",
        http_status: 503
    },
    e09: {
        success: false,
        msg: "No sufficient funds.",
        err_code: 9,
        severity: "warning",
        http_status: 401
    },
    e10: {
        success: false,
        msg: "No token provided.",
        err_code: 10,
        severity: "danger",
        http_status: 400
    },
    e11: {
        success: false,
        msg: "User token invalid.",
        err_code: 11,
        severity: "danger",
        http_status: 401
    },
    e12: {
        success: false,
        msg: "Old password not valid",
        err_code: 12,
        severity: "danger",
        http_status: 400
    },
    e13: {
        success: false,
        msg: "Error while modifying password",
        err_code: 13,
        severity: "dark",
        http_status: 500
    },
    e14: {
        success: false,
        msg: "Can't set the same password.",
        err_code: 14,
        severity: "warning",
        http_status: 400
    },
    e15: {
        msg: "No exchanges.",
        err_code: 15,
        severity: "infos",
        http_status: 200
    },
    e16: {
        success: false,
        msg: "3 or mores caraters needed",
        err_code: 16,
        severity: "danger",
        http_status: 400
    },
    e17: {
        success: false,
        msg: "The CAEL_id must be digits and 8 caraters long",
        err_code: 17,
        severity: "danger",
        http_status: 400
    },
    e18: {
        success: true,
        msg: "Password sucessfully changed!",
        err_code: 18,
        severity: "info",
        http_status: 200
    },
    e19: {
        success: false,
        msg: "You cannot delete a admin.",
        err_code: 19,
        severity: "danger",
        http_status: 401
    },
    e20: {
        success: false,
        msg: "ID has already been used by a deleted account.",
        err_code: 20,
        severity: "danger",
        http_status: 401
    },
    e21: {
        success: false,
        msg: "Unauthorized",
        err_code: 21,
        severity: "danger",
        http_status: 401
    },
    e22: {
        success: false,
        msg: "Error 404",
        err_code: 22,
        severity: "danger",
        http_status: 404
    },
    e23: {
        success: false,
        msg: "Revoked card. Please contact admin.",
        err_code: 23,
        severity: "danger",
        http_status: 403
    },
    e24: {
        success: false,
        msg: "Card number or code invalid.",
        err_code: 24,
        severity: "danger",
        http_status: 404
    },
    e25: {
        success: false,
        msg: "Card already assigned. Please contact admin.",
        err_code: 25,
        severity: "danger",
        http_status: 423
    },
    e26: {
        success: false,
        msg: "Nom d'utilisateur invalide. Entre 4 et 12 caractères alphanumérique.",
        err_code: 26,
        severity: "danger",
        http_status: 400
    },
    e27: {
        success: false,
        msg: "Email invalid.",
        err_code: 27,
        severity: "danger",
        http_status: 400
    },
    e28: {
        success: true,
        msg: "Card activated successfully!",
        err_code: 28,
        severity: "success",
        http_status: 201
    },
    e29: {
        success: false,
        msg: "Password invalid. It need to be 7 caraters long",
        err_code: 29,
        severity: "danger",
        http_status: 400
    },
    e30: {
        success: false,
        msg: "Invalid card. Please contact your reseller.",
        err_code: 30,
        severity: "danger",
        http_status: 400
    },
    e31: {
        success: false,
        msg: "Card not found or revoked.",
        err_code: 31,
        severity: "danger",
        http_status: 401
    },
    e32: {
        success: true,
        msg: "Card successfully changed!",
        err_code: 32,
        severity: "success",
        http_status: 200
    },
    e33: {
        success: false,
        msg: "Password do not match",
        err_code: 33,
        severity: "warning",
        http_status: 400
    },
    e34: {
        success: false,
        msg: "Unable to proceed. Card revoked.",
        err_code: 34,
        severity: "danger",
        http_status: 410
    },
    e35: {
        success: true,
        msg: "New card successfully assigned!",
        err_code: 35,
        severity: "success",
        http_status: 200
    },
    e36: {
        success: false,
        msg: "You already have a valid card. Change card in your dashboard.",
        err_code: 36,
        severity: "warning",
        http_status: 409
    },
    e37: {
        success: false,
        msg: "Error during process, please try again later.",
        err_code: 37,
        severity: "danger",
        http_status: 512
    },
    e38: {
        success: true,
        msg: "No content yet.",
        err_code: 39,
        severity: "info",
        http_status: 404
    },
    e39: {
        success: false,
        msg: "Bad request.",
        err_code: 39,
        severity: "danger",
        http_status: 400
    },
    e40: {
        success: false,
        msg: "Post not found.",
        err_code: 40,
        severity: "warning",
        http_status: 404
    },
    e41: {
        success: false,
        msg: "Card not found.",
        err_code: 41,
        severity: "danger",
        http_status: 404
    },
    e42: {
        success: false,
        msg: "Votre profil à été désactivé. Contactez un administrateur.",
        err_code: 42,
        severity: "danger",
        http_status: 401
    },
    e43: {
        success: false,
        msg: "Vous ne pouvez pas voter pour vous-même.",
        err_code: 43,
        severity: "danger",
        http_status: 403
    },
    e44: {
        success: false,
        msg: "Type de fichier non supporté. Merci de poster uniquement des images",
        err_code: 44,
        severity: "danger",
        http_status: 403
    },
    e45: {
        success: false,
        msg: "URL invalide",
        err_code: 45,
        severity: "danger",
        http_status: 400
    },
    e46: {
        success: false,
        msg: "Le code doit etre de 7 caractères",
        err_code: 46,
        severity: "warning",
        http_status: 400
    },
    e47: {
        success: false,
        msg: "Merci d'attendre 5 minutes entre chaques posts",
        err_code: 47,
        severity: "warning",
        http_status: 403
    },
    e48: {
        success: false,
        msg: "Le mail n'a pas pu être envoyé. Merci de réassayer plus tard",
        err_code: 48,
        severity: "danger",
        http_status: 512
    },
    e49: {
        success: false,
        msg: "Code ou email invalide",
        err_code: 49,
        severity: "danger",
        http_status: 404
    },
    e50: {
        success: false,
        msg: "Email introuvable",
        err_code: 50,
        severity: "danger",
        http_status: 404
    },
    e51: {
        success: false,
        msg: "Code expiré",
        err_code: 51,
        severity: "danger",
        http_status: 403
    },
    e99: {
        success: false,
        msg: "Server Error",
        err_code: 99,
        severity: "danger",
        http_status: 500
    }

});

exports.ErrorList = Error;
