/* Copyright © Swano 2019

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

The Software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the Software or the use or other dealings in the Software.

Except as contained in this notice, the name(s) of (the) Author shall not be used in advertising or otherwise to promote the sale, use or other dealings in this Software without prior written authorization from (the )Author.

*/

var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var functions = require('../functions')
var mongoosePaginate = require('mongoose-paginate');

var PostSchema = new Schema({
    created_by : {
        required: true,
        type: String
    },
    content: {
        title: {type: String, required: true},
        desc: String,
        pic_link: String,
        deletehash : String
    },
    created_at : {
        required: true,
        type: Number,
        unique: true,
        default: function () {
            return functions.getEpoch()
        }
    },
    points_elite: {
        type: Number,
        default: 0
    },
});

PostSchema.plugin(mongoosePaginate)

var QuotesTable = new Schema({
    quote: {
        type: String,
        required: true
    },
    author : {
        type: String,
        required: true
    },
    date : {
        type: String,
        required: true
    },
    registration_time: {
        type: Number,
        required: true,
        unique: true,
        default: function () {
            return functions.getEpoch()
        }
    }

})

var ResetTable = new Schema({
    email: {
        type: String,
        required: true
    },
    reset_time : {
        type: Number,
        required: true,
        default: function () {
            return functions.getEpoch()
        },
        unique: true
    },
    used_on: Number,
    reset_code: {
        type: String,
        required: true,
        default: function () {
            return functions.makeresetcode()
        }
    },
    ip: {
        type: String,
        required: true
    },
    used: {
        type: Boolean,
        required: true,
        default: false
    }
    
})

var CardsTable = new Schema({
    CAEL_id: {
        type: String,
        unique: true,
        required: true,
        index: true
    },
    registration_time: {
        type: Number,
        unique: true,
        index: true,
        required: true,
        default: function (){
           return functions.getEpoch()
        }
    },
    revoked: {
        type: Boolean,
        required: true,
        default: false
    },
    type: {
        type: String,
        required: true,
    },
    assign: {
        type: Boolean,
        required: true,
        default: false
    },
    CAEL_code: {
        type: String,
        required: true,
        default: function (){
            return functions.makeid()
        }
    },
    departement: {
        type: String,
        required: true
    },
    exp_date: {
        type: Number,
        required: true,
        default: function (){
            return functions.getEpoch() + 31536000 // Un an
        }
    },
    user_id: String
})

CardsTable.plugin(mongoosePaginate)

exports.ResetTable = mongoose.model('ResetTable', ResetTable, 'resets_table')
exports.QuotesTable = mongoose.model('QuotesTable', QuotesTable, 'quotes_table')
exports.PostSchema = mongoose.model('PostSchema', PostSchema, 'posts_table')
exports.CardsTable = mongoose.model('CardsTable', CardsTable, 'cards_table')
