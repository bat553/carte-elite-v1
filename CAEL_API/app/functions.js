/*
    functions
    CAEL

    Created by Swano 21/01/19 18:19
*/
var jwt = require('jsonwebtoken'),
    config = require('./config/db')

module.exports = {

    getToken : function (headers) {
            if (headers && headers.authorization) {
                var parted = headers.authorization.split(' ');
                if (parted.length === 2) {
                    return parted[1];
                } else {
                    return null;
                }
            } else {
                return null;
            }
    },

    getEpoch: function() {
        return Math.round((new Date).getTime()/1000)
    },

    getCardInfos: function (CAEL_id, type) {
        // Détermine le type de carte en suivant le fichier CAEL_code.txt
        // Determine le type (aa BB cc dd)

        if (type == "model") {
            switch (CAEL_id.substr(2, 2)) {
                case "00":
                    return "edition";
                    break;
                case "04":
                    return "standard";
                    break;
                case "05":
                    return "gold";
                    break;
                case "06":
                    return "platinum";
                    break;
                case "07":
                    return "vingt-et-un";
                    break;
                case "99":
                    return "test";
                    break;
                default:
                    return null
            }
        } else if (type == "departement") {
            // Determine le departement (aa bb CC dd)
            switch (CAEL_id.substr(4, 2)) {
                case "00":
                    return "RT";
                    break;
                case "01":
                    return "GMP";
                    break;
                case "02":
                    return "GEII";
                    break;
                case "03":
                    return "GCCD";
                    break;
                case "51":
                    return "test";
                    break;
                default:
                    return null;
            }
        } else if (type == "exp") {
            return this.getEpoch() + 31536000 // Carte valide 1 ans
        } else {
            return null
        }
        // Retourne sous forme d'un tableau
    },

    makeid: function() {
        // https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 7; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    },

    makeresetcode: function() {
        // https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 35; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    },

    getDecodedToken: function (headers) {
        var token = this.getToken(headers);
        if (token) {
            return jwt.verify(token, config.secret);
        }else {
            return null
        }
    }
}

