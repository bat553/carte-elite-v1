/*
    public_routes
    CAEL

    Created by Swano 21/01/19 17:31
*/
var apiconfig = require('../../config/api')
var validator = require('validator');
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer')
var functions = require("../../functions");
var User = require('../../models/user');
var Error = require('../../models/errors').ErrorList;
var DataSet = require("../../models/datas");
var config = require('../../config/db'); // BDD CAEL_USERS_OP
var CardsTable = DataSet.CardsTable;
var PostSchema = DataSet.PostSchema
var QuotesTable = DataSet.QuotesTable
var ResetTable = DataSet.ResetTable

// Setup mailer
var transporter = nodemailer.createTransport({
    host: "in-v3.mailjet.com",
    port: 587,
    secure: false,
    auth: {
        user: "0cb1e19dcdb8ff4e43a240129456ef1b",
        pass: "6e1857db9d6bffcf2f281744279f7b6f"
    }
})

var mailOptions = {
    from : "La carte élite <no-reply@elite21.fr>",
    subject: "Réinitialisation de votre mot de passe."
}


module.exports = function(app) {

    app.post(apiconfig.api_url + 'reset/', function (req, res) {
        // Reset le mdp de l'utilisateur
        // Si demande de reset
        if (!req.body.email || !validator.isEmail(req.body.email)){
            // Si pas d'email ou email incorect
            res.status(Error.e03.http_status).send(Error.e03)
        } else {
            // Anti flood
            ResetTable.findOne({email: req.body.email}).sort({reset_time: -1}).exec(function (err, last) {
                if (err){
                    res.status(Error.e99.http_status).send(Error.e99)
                } else if (last && (functions.getEpoch() - last.reset_time) < 600){
                    // Si le temps entre les request n'est pas respecté
                    res.status(Error.e47.http_status).send({
                        success: false,
                        msg: "Merci d'attendre 5 minutes entre chaques envoie de mails.",
                        err_code: 47,
                        temps_restant: (functions.getEpoch() - last.reset_time - 300)/-1,
                        severity: "warning",
                        http_status: 403
                    })
                } else {
                    // Sinon all good
                    // https://stackoverflow.com/questions/8107856/how-to-determine-a-users-ip-address-in-node
                    var reset_query = new ResetTable({
                        ip: req.headers['CF-Connecting-IP'] || req.connection.remoteAddress,
                        email : req.body.email,
                        reset_code: functions.makeresetcode()
                    })
                    mailOptions['to'] = req.body.email;
                    mailOptions['html'] =
                        "<div style='text-align: center; border: grey 1px solid; border-radius: 3px'>" +
                        "<h1 style='text-decoration: underline; color: #e2ad46; font-family: Roboto, Arial, sans-serif'>Votre carte Élite</h1>" +
                        "<h3>Vous avez demandé la réinitialisation de votre mot de passe</h3>" +
                        "<h5>Si ce n'est pas le cas, vous pouvez ignorer cet email.</h5>" +
                        "<a href=\"https://elite21.fr/requestpwd/"+reset_query.reset_code+"\">Réinitialiser votre mot de passe</a>" +
                        "<br>" +
                        "<code>Ce lien n'est valide que 5 minutes.</code>" +
                        "<h6><a href='https://elite21.fr/'>elite21.fr</a></h6>" +
                        "</div>";
                    // verification si l'email existe
                    User.findOne({email: req.body.email}, function (err, user) {
                        if (err){
                            res.status(Error.e99.http_status).send(Error.e99)
                        } else if (!user) {
                            // Si pas d'email
                            res.status(Error.e50.http_status).send(Error.e50)
                        } else {
                            // All good
                                var info = transporter.sendMail(mailOptions, function (err, info) {
                                    if (err || !info){
                                        console.log(err)
                                        res.status(Error.e48.http_status).send(Error.e48)
                                    } else {
                                        reset_query.save(function (err, reset) {
                                            if (err || !reset) {
                                                res.status(Error.e99.http_status).send(Error.e99)
                                            } else {
                                                res.status(200).send({
                                                    success: true,
                                                    msg: "Le mail de réinitialisation à été envoyé.",
                                                    severity: "success"
                                                })
                                            }
                                        })
                                    }
                                })

                        }
                    })
                }
            })
        }
    })


    app.get(apiconfig.api_url + 'card/:CAEL_id', function (req, res) {
        // Renvoie des infos de base sur le possesseur d'une carte
        CardsTable.findOne({CAEL_id: req.params.CAEL_id, $and: [{assign: true}, {revoked: false}]}, function (err, card) {
            if (!card || err){
                res.status(Error.e41.http_status).send(Error.e41)
            } else {
                User.findOne({username: card.user_id}, function (err, user) {
                    // Trouve l'utilisateur
                    if (!user || err){
                        res.status(Error.e41.http_status).send(Error.e41)
                    } else {
                        // Trouve les posts
                        PostSchema.find({created_by: user.username}, function (err, posts) {
                            res.status(200).send({
                                success: true,
                                username: user.username,
                                card_id: card.CAEL_id,
                                posts: posts
                            })
                        })

                    }
                })
            }
        })
    })


    app.get(apiconfig.api_url + 'dev/commit', function (req, res) {
        // Récupère l'id du commit (uniquement en dev)
        if (process.env.NODE_ENV == "developpement") {
            try {
                if (!process.env.COMMIT){
                    res.status(500).send({success: false})
                } else {
                    res.send({success: true, display: "d-block", commit: process.env.COMMIT})
                }
            } catch (e) {
                res.status(Error.e99.http_status).send(Error.e99)
            }
        }
        else if (process.env.NODE_ENV == "dev"){
            res.status(200).send({success: true, display: "d-block",commit: "devruntime"})
        }
        else {
            res.status(200).send({success: true, display: "d-none", commit: "production"});
        }
    })

    app.post(apiconfig.api_url + 'auth', function (req, res) {
        if (!req.body.username || !req.body.password) {
            res.status(Error.e03.http_status).send(Error.e03);
        }
        else {
            User.findOne({
                $or: [
                    {username: (req.body.username).toLowerCase()}, // Soit l'username
                    {email: (req.body.username).toLowerCase()}] // Soit l'email
            }, {__v: 0, voted_posts: 0, posts: 0}, function (err, user) {
                if (err) throw err;

                if (!user) {
                    res.status(Error.e05.http_status).send(Error.e05)
                }
                else {
                    // check password match
                    user.comparePassword(req.body.password, function (err, isMatch) {
                        if (isMatch && !err) {
                            if (user.disable == true){
                                // Si l'utilisateur est désactivé
                                res.status(Error.e42.http_status).send(Error.e42)
                            } else {
                                // if user found and password is right, verify card revokation and assignation
                                CardsTable.findOne({$and: [{user_id: user.username}, {revoked: false}, {assign: true}]}, {__v: 0, CAEL_code: 0, _id: 0}, function (err, card) {
                                    // TODO : Verifie l'expiration
                                    if (card) {
                                        var token_plain = {user: user.toJSON(), card: card.toJSON()};
                                        var token = jwt.sign(token_plain, config.secret, {expiresIn: '30m'});
                                        // Change last connection field
                                        User.findOneAndUpdate({username: user.username}, {lastLogin: functions.getEpoch()}, function (err, userup) {
                                            res.send({success: true, token: token});
                                        })
                                    } else {
                                        res.status(Error.e31.http_status).send(Error.e31)
                                    }
                                })
                            }
                        } else {
                            res.status(Error.e05.http_status).send(Error.e05);
                        }
                    });
                }
            });
        }
    });

    app.get(apiconfig.api_url + 'public/stats', function (req, res) {
        // Recupères les données publiques sur CAEL
        try {
            var posts_count = new Promise(function (resolve) {
                resolve(PostSchema.countDocuments());
            });
            var cards_count = new Promise(function (resovle) {
                    resovle(CardsTable.countDocuments());
            });
            var users_count = new Promise(function (resovle) {
                    resovle(User.countDocuments());
            });
            var online_cards_count = new Promise(function (resovle) {
                    resovle(CardsTable.find({$and:[{assign:true}, {revoked: false}]}).countDocuments());
            });



            Promise.all([posts_count, cards_count, users_count, online_cards_count]).then(function (values) {
                res.send({success: true, posts_count: values[0], cards_count: values[1], users_count: values[2], online_cards_count: values[3]})
            }).catch(function (err) {
                res.status(Error.e99.http_status).send(Error.e99, err)
            });
        } catch (e) {
            res.status(Error.e99.http_status).send(Error.e99, e)
        }

    });

    app.get(apiconfig.api_url + 'quote', function (req, res) {
        QuotesTable.count().exec(function (err, count) {
            if (err || !count){
                res.status(Error.e24.http_status).send(Error.e24)
            } else {
                var random = Math.floor(Math.random() * count);
                QuotesTable.findOne()
                    .select({_id: 0, registration_time: 0, __v:0})
                    .skip(random)
                    .exec(
                        function (err, quote) {
                        res.status(200).send(quote)
                })
            }
        })
    })

    app.post(apiconfig.api_url + 'activate', function (req, res) {
        // Ajoute une nouvelle carte au parc (deplacement de Cards vers users)
        if (req.body.CAEL_id && req.body.username && req.body.password && req.body.email && req.body.confirm_password && req.body.CAEL_code && req.body.gender && req.body.birthday && req.body.lastname && req.body.surname){
            CardsTable.findOne({
                CAEL_id: req.body.CAEL_id,
                // Si la carte exite
                $and: [
                    {CAEL_code: req.body.CAEL_code
                        // Et si le code correspond
                    }]}, function (err, card) {
                if (err) throw err;
                if(card){
                    // Carte trouvée
                    // Verification si elle n'est pas révoquée
                    if(card.revoked){
                        res.status(Error.e23.http_status).send(Error.e23);
                    }
                    else if (req.body.password !== req.body.confirm_password){
                        res.status(Error.e33.http_status).send(Error.e33)
                    }
                    else if (card.assign){
                        res.status(Error.e25.http_status).send(Error.e25)
                    }
                    else if ((((req.body.username).length >= 12) || ((req.body.username).length <= 4))){
                        // si le pseudo contien des numeros ou des accent et si il est plus grand ou egal à 11 caractères et < a 5
                        res.status(Error.e26.http_status).send(Error.e26)
                    }
                    else if (!validator.isEmail(req.body.email)){
                        // si l'email est invalide
                        res.status(Error.e27.http_status).send(Error.e27)
                    }
                    else if ((req.body.password).length < 7){
                        // si le mdp est < à 7 alors invalide
                        res.status(Error.e29.http_status).send(Error.e29)
                    }
                    else {
                        // Verification si elle n'est pas présente dans User ou transaction table
                        User.findOne({$or:[{username: req.body.username},{email: req.body.email}]}, function (err, user) {
                            if (err) throw err;
                            if (user) {
                                res.status(Error.e06.http_status).send(Error.e06)
                            } else {
                                var exist_user = new Promise(function (resolve, reject) {
                                    // Verification si elle n'est pas présente dans User
                                    resolve(User.findOne(
                                        {username: req.body.username}
                                    ));
                                    reject();
                                })
                                var exist_transaction = new Promise(function (resolve, reject) {
                                    // Verification si elle n'est pas présente dans Transaction Table
                                    resolve(TransactionsTable.findOne({CAEL_id: req.body.CAEL_id}))
                                    reject();
                                    // BUT: Rendre faux si il trouve une entrée
                                })
                                Promise.all([exist_transaction, exist_user]).then(function (value) {
                                    if (!value[0] && !value[1]){throw "All good"}
                                    res.status(Error.e06.http_status).send(Error.e06);
                                }).catch(function () {
                                    // Si rien n'est touvé, continuer
                                    var NewUser = new User({
                                        username: (req.body.username).toLowerCase(),
                                        password: req.body.password,
                                        email: (req.body.email).toLowerCase(),
                                        lastname: req.body.lastname,
                                        surname: req.body.surname,
                                        admin: false,
                                        gender: req.body.gender,
                                        birthday: req.body.birthday
                                    });
                                    NewUser.save(function (err) {
                                        if (err) {
                                            if (err.code == 11000) {
                                                res.status(Error.e20.http_status).send(Error.e20)
                                            } else {
                                                res.status(Error.e06.http_status).json(Error.e06)
                                            }
                                        } else {
                                            CardsTable.findOneAndUpdate({CAEL_id: req.body.CAEL_id}, {assign: true, user_id: NewUser.username}, function (errT, doc) {
                                                if (errT) {
                                                    // Si il y  a une erreur rollback l'inscription
                                                    User.findOneAndRemove({CAEL_id: req.body.CAEL_id}, function (err, doc) {
                                                        if (err){
                                                            // Si probleme dans l'update, rollback
                                                            res.status(512).send({success: false, msg: err})
                                                        } else{
                                                            res.status(512).send({success: false, msg: errT})
                                                        }
                                                    })
                                                } else {
                                                    User.findOne({username: req.body.username}, function (err, user) {
                                                        if (user){
                                                            res.status(Error.e28.http_status).send(Error.e28);
                                                        } else {
                                                            // Si l'utilisateur n'a pas été enregisté, error
                                                            res.status(512).send({success: false, msg: err})
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })

                                })
                            }
                        })
                    }
                } else res.status(Error.e24.http_status).send(Error.e24)

            })


        }else {
            // Manque un field
            res.status(Error.e03.http_status).send(Error.e03);
        }
    });

    app.post(apiconfig.api_url + 'link', function (req, res) {
        // Liaison d'une nouvelle carte à un compte existant
        if (req.body.CAEL_id || req.body.CAEL_code || req.body.username || req.body.password){
            // Si il manque un champ
            // Verification de l'utilisateur (pas désactivé)
            User.findOne({username: (req.body.username).toLowerCase()}, function (err, user) {
                if (err || !user) {res.status(Error.e05.http_status).send(Error.e05)}
                else if (user.disable){
                    // Si l'utilisateur est désacivé
                    res.status(Error.e42.http_status).send(Error.e42)
                } else {
                    // Utilisateur trouvé, verification password
                    user.comparePassword(req.body.password, function (err, isMatch) {
                        if (isMatch && !err) {
                            // If user has already a card
                            CardsTable.findOne({$and:[{user_id: (req.body.username).toLowerCase()}, {assign: true}, {revoked: false}]}, function (err, u_card) {
                                if (err || u_card){
                                    // Si l'utilisateur possede déja une carte non révoquée et fonctionnelle
                                    res.status(Error.e36.http_status).send(Error.e36)
                                }else {
                                    // if user found and password is right, verify card revokation and assignation
                                    CardsTable.findOne({CAEL_id: req.body.CAEL_id, $and: [{CAEL_code: req.body.CAEL_code}]}, function (err, card) {
                                        if (err || !card){
                                            res.status(Error.e24.http_status).send(Error.e24)
                                        }
                                        else {
                                            // si carte trouvée
                                            // Verification si elle n'est pas révoquée
                                            if(card.revoked){
                                                res.status(Error.e23.http_status).send(Error.e23);
                                            }
                                            // Si la carte est assignée
                                            else if (card.assign){
                                                res.status(Error.e25.http_status).send(Error.e25)
                                            } else {
                                                // Si carte dispo et non revoquée
                                                // Assignation à l'utilisateur
                                                CardsTable.findOneAndUpdate({CAEL_id: req.body.CAEL_id}, {assign: true, user_id: (req.body.username).toLowerCase()}, function (err, confirm) {
                                                    if (err) {
                                                        res.status(Error.e99.http_status).send(Error.e99)
                                                    }   else {
                                                        res.status(Error.e35.http_status).send(Error.e35)
                                                    }
                                                })
                                            }
                                        }
                                    })
                                }
                            })
                        } else {
                            res.status(Error.e05.http_status).send(Error.e05);
                        }
                    });
                }
            })
        } else {
            res.status(Error.e03.http_status).send(Error.e03);
        }

    })
}
