var apiconfig = require('../config/api')
var Error = require('../models/errors').ErrorList


module.exports = function (app) {
    //index route
    app.get(apiconfig.api_url, function (req, res) {
        res.send({success: true, msg: "Welcome to the CAEL_API ! " + apiconfig.api_version})
    });

    app.get('/', function (req, res) {
        res.send({success: true, msg: "Welcome to the CAEL_API ! " + apiconfig.api_version})
    })

     // Always at the end of the file
    app.use(function (req, res, next) {
         // Erreur 404
         res.status(Error.e22.http_status).send(Error.e22)
    })

};

