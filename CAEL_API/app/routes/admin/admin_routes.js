/*
    admin_routes
    CAEL

    Created by Swano 21/01/19 18:12
*/
var apiconfig = require('../../config/api')
var jwt = require('jsonwebtoken');
var passport = require("passport");
require("../../config/passport")(passport);
var User = require('../../models/user');
var functions = require("../../functions");
var Error = require('../../models/errors').ErrorList;
var config = require('../../config/db'); // BDD CAEL_USERS_OP
var DataSet = require("../../models/datas");
var CardsTable = DataSet.CardsTable;
var QuoteTable = DataSet.QuotesTable;

function isAuth(req, res, next) {
    var token = functions.getToken(req.headers);
    if (token){
        try {
            var decoded_token = jwt.verify(token, config.secret);
            User.findOne({
                $and: [
                    {username: decoded_token.user.username},
                    {password: decoded_token.user.password},
                    {admin: true},
                    {disable: false}
                ]
            }, function (err, user, decoded) {
                if (!user || err){
                    res.status(Error.e11.http_status).send(Error.e11)
                } else {
                    CardsTable.findOne({$and:[
                            // Carte non révoquée et assignée
                            {user_id: decoded_token.user.username},
                            {revoked: false},
                            {assign: true}
                        ]}, function (err, card) {
                        if (!err && card && card.CAEL_id == decoded_token.card.CAEL_id) {
                            // Si on utilise bien la bonne carte
                            return next();
                        }else {
                            res.status(Error.e31.http_status).send(Error.e31)
                        }
                    })
                }
            });
        } catch (e) {
            res.status(Error.e11.http_status).send(Error.e11)
        }
    } else {
        res.status(Error.e10.http_status).send(Error.e10)
    }
}

module.exports = function (app) {
    app.put(apiconfig.api_url + 'admin/add_card', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
            if (req.body.CAEL_id){
                if (isNaN(req.body.CAEL_id) || (req.body.CAEL_id).length != 8) {
                    // Si ce n'est pas un chiffre ou si ca taille n'est pas égale a 8
                    res.status(Error.e17.http_status).send(Error.e17);
                } else {
                    var newCard = new CardsTable({
                        type: functions.getCardInfos(req.body.CAEL_id, "model"),
                        departement: functions.getCardInfos(req.body.CAEL_id, "departement"),
                        CAEL_id: req.body.CAEL_id
                        // Clean, default parameters
                    });
                    CardsTable.findOne({CAEL_id: req.body.CAEL_id}, function (err, cards) {
                        if (cards || err) {
                            res.status(Error.e06.http_status).send(Error.e06); // La carte est déja insérée
                        } else {
                            if (newCard.type && newCard.departement) {
                                newCard.save(function (err) {
                                    if (err) {
                                        res.status(500).send({success: false, msg: err});
                                    } else {
                                        res.status(201).send({
                                            success: true,
                                            msg: "Card no " + req.body.CAEL_id + " registred!",
                                            CAEL_code: newCard.CAEL_code,
                                            type: newCard.type,
                                            departement: newCard.departement,
                                            registration_time: newCard.registration_time,
                                            exp_date: newCard.exp_date
                                        })
                                    }
                                })
                            } else {
                                res.status(Error.e30.http_status).send(Error.e30);
                            }
                        }
                    })
                }
            } else {
                res.status(Error.e03.http_status).send(Error.e03);
            }
    })

    app.post(apiconfig.api_url + "admin/add_quote", isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        if (req.body.author && req.body.quote && req.body.date){
            var newQuote = new QuoteTable({
                quote: req.body.quote,
                author: req.body.author,
                date : req.body.date
            })
            newQuote.save(function (err) {
                if (err){
                    req.status(Error.e99.http_status).send(Error.e99)
                } else {
                    res.status(201).send({
                        success: true,
                        msg: "Quote added ! \" " + req.body.quote + " \" by : " + req.body.author + " on : " + req.body.date
                    })
                }
            })
        } else {
            res.status(Error.e03.http_status).send(Error.e03)
        }

    })

    app.post(apiconfig.api_url + 'admin/get_card', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
            // Admin trouvé
            CardsTable.findOne({CAEL_id: req.body.CAEL_id}, function (err, card) {
                if (err || !card){
                    res.status(Error.e22.http_status).send(Error.e22)
                } else {
                    //Carte valide trouvé, affichage des infos
                    res.status(200).send({success: true, msg: "Card found!", card_infos: card})
                }

            })
    })

    app.get(apiconfig.api_url + 'admin/card/:id', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        // Recupére les numéros de carte à la vollée (+ de 3 chiffres)
        if (req.params.id && (req.params.id).length > 3) {
            CardsTable.find({CAEL_id: new RegExp(req.params.id)}, function (err, cards) {
                if (err){
                    res.status(Error.e99.http_status).send(Error.e99)
                } else if (!cards){
                    res.status(Error.e41.http_status).send(Error.e41)
                } else {
                    res.status(200).send({success: true, cards: cards})
                }
            })
        }else {
            res.status(Error.e03.http_status).send(Error.e03)
        }

    })

    app.get(apiconfig.api_url + 'admin/users/list', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        // Répupere la liste des utilisateurs
        User.paginate({}, {select:{_id: 0, __v: 0, voted_posts: 0, posts: 0, password: 0}, sort:{registration_time: -1}}, function (err, users) {
            if (err ||!users){
                res.status(Error.e24.http_status).send(Error.e24)
            }else {
                res.status(200).send({success: true, users: users})
            }

        })

    })

    app.post(apiconfig.api_url + 'admin/update/card', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        // Update le status d'une carte élite
        var decoded_token = functions.getDecodedToken(req.headers)
        if (decoded_token){
            // Paramètres obligatoires
            if (req.body.CAEL_id && (req.body.CAEL_code || req.body.revoked || res.body.assign)){
                // Current pramaters
                if (req.body.revoked == "Non"){
                    revoked = "false"
                } else if (req.body.revoked == "true"){
                    revoked = "true"
                } else {revoked = null}

                // TODO : Fix this (error 500 quand pas de paramètres)
                if (req.body.assign == "Non"){
                    assign = "false"
                } else if (req.body.assign == "true"){
                    assign = "true"
                } else {assign = null}

                if (req.body.CAEL_code && req.body.CAEL_code.length != 7){
                    // Si le code ne fait pas 7 caratères de long
                    res.status(Error.e46.http_status).send(Error.e46)
                } else {
                    CardsTable.findOne({CAEL_id: req.body.CAEL_id}, function (err, card) {
                        if (!card) {
                            res.status(Error.e41.http_status).send(Error.e41)
                        } else {
                            var query = {
                                // Updates uniquement les champs changés
                                revoked: revoked || card.revoked,
                                assign: assign || card.assign,
                                user_id: req.body.user_id || card.user_id || null,
                                CAEL_code: req.body.CAEL_code || card.CAEL_code
                            }
                            CardsTable.findOneAndUpdate({CAEL_id: req.body.CAEL_id}, query, {new: true}, function (err, upd) {
                                if (err) {
                                    res.status(Error.e99.http_status).send(Error.e99)
                                } else if (!upd) {
                                    res.status(Error.e41.http_status).send(Error.e41)
                                } else {
                                    res.status(200).send({
                                        success: true,
                                        severity: "success",
                                        new_doc: upd,
                                        msg: "Update successfull !"
                                    })
                                }
                            })
                        }
                    })
                }
            } else {
                res.status(Error.e03.http_status).send(Error.e03)
            }

        } else {
            res.status(Error.e10.http_status).send(Error.e10)
        }

    })

    app.get(apiconfig.api_url + 'admin/cards/:user_id?/:revoked?/:assign?/:dep?/:type?/:page?/', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        // Récupération de la liste des cartes dans la table avec pagination
        query = {}
        if (req.params.user_id && req.params.user_id !== "null"){
            query['user_id'] = req.params.user_id
        }
        if (req.params.assign && req.params.assign != "null"){
            if (req.params.assign == "Non"){
                query['assign']= (false)
            } else {
                query['assign'] = req.params.assign
            }
        }
        if (req.params.revoked && req.params.revoked != "null" ){
            if (req.params.revoked == "Non"){
                query['revoked']= (false)
            } else {
                query['revoked'] = (req.params.revoked)
            }
        }
        if (req.params.type && req.params.type !== "null"){
            query['type']= (req.params.type)
        }
        if (req.params.dep && req.params.dep !== "null"){
            query['departement']= (req.params.dep)
        }
        CardsTable.paginate(query, {
            sort: {registration_time: -1},
            page: req.params.page || 1,
            limit: 10
        }, function (err, cards) {
                if (err) {
                    res.status(Error.e99.http_status).send(Error.e99)
                } else if (!cards) {
                    // Return : pas de contenu
                    res.status(Error.e38.http_status).send(Error.e38)
                } else {
                    res.status(200).send({success: true, cards: cards})
                }
        })

    })


}