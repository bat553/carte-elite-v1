/*
    dev_routes
    CAEL

    Created by Swano 21/01/19 18:14
*/

var jwt = require('jsonwebtoken');
var apiconfig = require('../../../config/api')
var User = require('../../../models/user');
var config = require('../../../config/db'); // BDD CAEL_USERS_OP
var DataSet = require("../../../models/datas");
var functions = require("../../../functions");
var Error = require('../../../models/errors').ErrorList;
var CardsTable = DataSet.CardsTable;

module.exports = function (app) {

    app.post(apiconfig.api_url + 'TEMP/auth', function (req, res) {
        // Retire l'expiration du token
        if (process.env.NODE_ENV === "dev") {
            if (!req.body.username) {
                res.status(Error.e03.http_status).send(Error.e03);
            } else {
                User.findOne({username: req.body.username}, function (err, user) {
                    if (err) throw err;

                    if (!user) {
                        Error.e05.msg = Error.e05.msg;
                        res.status(Error.e05.http_status).send(Error.e05)
                    } else {
                        // check password match
                        user.comparePassword(req.body.password, function (err, isMatch) {
                            if (isMatch && !err) {
                                // if user found and password is right, create a token
                                var token = jwt.sign({user: user.toJSON()}, config.secret);

                                if (user) {
                                    res.send({success: true, token: token});
                                }
                            } else {
                                res.status(Error.e05.http_status).send(Error.e05);
                            }
                        });
                    }
                });
            }
        }
    });

    app.put(apiconfig.api_url + 'admin/TEMP/add_card', function (req, res) {
        if (process.env.NODE_ENV === "dev") {
            if (req.body.CAEL_id) {
                if (isNaN(req.body.CAEL_id) || (req.body.CAEL_id).length != 8) {
                    // Si ce n'est pas un chiffre ou si ca taille n'est pas égale a 8
                    res.status(Error.e17.http_status).send(Error.e17);
                } else {
                    var newCard = new CardsTable({
                        type: functions.getCardInfos(req.body.CAEL_id, "model"),
                        departement: functions.getCardInfos(req.body.CAEL_id, "departement"),
                        CAEL_id: req.body.CAEL_id,
                        registration_time: functions.getEpoch(),
                        revoked: false,
                        assign: false,
                        exp_date: functions.getCardInfos(req.body.CAEL_id, "exp"),
                        CAEL_code: functions.makeid()  // Code sur 8 strings random,
                    });
                    CardsTable.findOne({CAEL_id: req.body.CAEL_id}, function (err, cards) {
                        if (cards || err) {
                            res.status(Error.e06.http_status).send(Error.e06); // La carte est déja insérée
                            res.end()
                        } else {
                            if (newCard.type && newCard.departement) {
                                newCard.save(function (err) {
                                    if (err) {
                                        res.status(500).send({success: false, msg: err});
                                        res.end()
                                    } else {
                                        res.status(201).send({
                                            success: true,
                                            msg: "Card no " + req.body.CAEL_id + " registred!",
                                            code: newCard.CAEL_code,
                                            type: newCard.type,
                                            departement: newCard.departement,
                                            registration_time: newCard.registration_time,
                                            exp_date: newCard.exp_date
                                        })
                                    }
                                })
                            } else {
                                res.status(Error.e30.http_status).send({
                                    err: Error.e30,
                                    dep: newCard.dep,
                                    type: newCard.type
                                });
                            }
                        }
                    })
                }
            } else {
                res.status(Error.e03.http_status).send(Error.e03);
            }
        }
    })




}