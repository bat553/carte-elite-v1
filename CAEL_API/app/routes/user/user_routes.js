/*
    user_routes
    CAEL

    Created by Swano 21/01/19 18:36
*/

var jwt = require('jsonwebtoken');
var passport = require('passport');
var validator = require('validator');
require("../../config/passport")(passport);
var apiconfig = require('../../config/api')
var User = require('../../models/user');
var functions = require("../../functions");
var Error = require('../../models/errors').ErrorList;
var config = require('../../config/db'); // BDD CAEL_USERS_OP
var DataSet = require("../../models/datas");
var CardsTable = DataSet.CardsTable;
var PostSchema = DataSet.PostSchema;
var bcrypt = require('bcrypt-nodejs');
var BCRYPT_SALT_ROUNDS = 10;
var formidable = require('formidable')
var imgur = require('imgur')
var ResetTable = DataSet.ResetTable


imgur.setClientId("3f08cde671427ee")


function isAuth(req, res, next) {
    var token = functions.getToken(req.headers);
    if (token){
        try {
            var decoded_token = jwt.verify(token, config.secret);
            User.findOne({
                $and: [
                    {username: decoded_token.user.username},
                    {password: decoded_token.user.password},
                    {disable: false}
                ]
            }, function (err, user, decoded) {
                if (!user || err){
                    res.status(Error.e11.http_status).send(Error.e11)
                } else {
                    CardsTable.findOne({$and:[
                            // Carte non révoquée et assignée
                            {user_id: decoded_token.user.username},
                            {revoked: false},
                            {assign: true}
                        ]}, function (err, card) {
                        if (!err && card && card.CAEL_id == decoded_token.card.CAEL_id) {
                            // Si on utilise bien la bonne carte
                            return next();
                        }else {
                            res.status(Error.e31.http_status).send(Error.e31)
                        }
                    })
                }
            });
        } catch (e) {
            res.status(Error.e11.http_status).send(Error.e11)
        }
    } else {
        res.status(Error.e10.http_status).send(Error.e10)
    }
}

module.exports = function (app) {

    app.post(apiconfig.api_url+"user/reset", function (req, res) {
        // Reset le mdp avec le code envoyé par mail
        if (!req.body.reset_code || !req.body.password || !req.body.c_password || !req.body.email || !validator.isEmail(req.body.email)){
            // Si manque un des champs ou pas une email
            res.status(Error.e03.http_status).send(Error.e03)
        } else if (req.body.password !== req.body.c_password){
            res.status(Error.e33.http_status).send(Error.e33)
        }else {
            // All good
            ResetTable.findOne({$and:[{email: req.body.email}, {reset_code: req.body.reset_code}, {used: false}]}, function (err, reset) {
                if (err){
                    res.status(Error.e99.http_status).send(Error.e99)
                } else if (!reset){
                    // Pas de code trouvé (404)
                    res.status(Error.e49.http_status).send(Error.e49)
                } else if (functions.getEpoch() - reset.reset_time > 300){
                  // Si le code est vieux de plus de 5 minutes
                  res.status(Error.e51.http_status).send(Error.e51)
                } else {
                    // All good, reset du mdp
                    bcrypt.hash(req.body.password, bcrypt.genSaltSync(BCRYPT_SALT_ROUNDS), null, function (err, hash) {
                            if (err){
                                res.status(Error.e13.http_status).send(Error.e13)
                            }
                            else {
                                User.findOneAndUpdate({email: req.body.email}, {password: hash}, function (err, doc) {
                                        if (err) {
                                            res.status(Error.e99.http_status).send(Error.e99, {msg: err, doc: doc})
                                        }else {
                                            // Desactivation du code et fin
                                         ResetTable.findOneAndUpdate({reset_code: req.body.reset_code}, {used: true, used_on: functions.getEpoch()}, function (err, info) {
                                             if(err || !info){
                                                 res.status(Error.e99.http_status).send(Error.e99)
                                             }else {
                                                 res.status(200).send({
                                                     success: true,
                                                     msg: "Mot de passe mis a jour avec succès.",
                                                     severity: "success"
                                                 });
                                             }
                                         })
                                        }
                                    });
                            }

                    })
                }

            })

        }
    })

    app.post(apiconfig.api_url + 'user/update', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        decoded_token = functions.getDecodedToken(req.headers)
        var form = formidable.IncomingForm()
        query = {}
        form.parse(req, function (err, fields, files) {
            if (files.avatar){
                // Changment d'avatar
                // Si deja un avatar, retirer
                User.findOne({username: decoded_token.user.username}, function (err, user) {
                    imgur.deleteImage(user.deletehash)
                    imgur.uploadFile(files.avatar.path)
                        .then(function (json) {
                            if (json.data.link){
                                query['avatar_link'] = json.data.link
                                query['deletehash'] = json.data.deletehash
                            }
                            User.findOneAndUpdate({username: decoded_token.user.username}, query, function (err, update) {
                                if (err || !update){
                                    res.status(Error.e99.http_status).send(Error.e99)
                                } else{
                                    res.status(200).send({success: true, user: update})
                                }
                            })

                        }).catch(function (err) {
                        res.status(512).send({success: false, msg: err.message})
                    })
                })
            }else{
                res.status(Error.e03.http_status).send(Error.e03)
            }
        })

    })

    app.post(apiconfig.api_url + 'user/post', isAuth, passport.authenticate('jwt', {session: false}), function (req, res, next) {
        // Ajoute un post à la bdd
        // Verification deja effectuée (isAuth)
        var decoded_token = functions.getDecodedToken(req.headers)
        var form = formidable.IncomingForm()
        form.parse(req, function (err, fields, files) {
            if (fields.pic_url){
                // Si la photo est un lien
                console.log('lien', fields.pic_url)
                post = new Promise(function (resolve) {
                    imgur.uploadUrl(fields.pic_url)
                        .then(function (json) {
                            console.log(json)
                            var link = json.data.link
                            var deletehash = json.data.deletehash
                            // Post avec une photo via url

                            resolve(new PostSchema({
                                created_by: decoded_token.user.username,
                                content: {
                                    title: fields.title,
                                    desc: fields.desc || null,
                                    pic_link: link,
                                    deletehash: deletehash
                                }
                            }));
                        }).catch(function (err) {
                        res.status(Error.e45.http_status).send(Error.e45)
                    })
                })
            }
            // Si la photo est directement présente sur le post (upload)
            else if (files.image) {
                if (fields.title == "bad"){ // Debug
                    res.status(Error.e44.http_status).send(Error.e44)
                } else {
                    post = new Promise(function (resolve) {
                        imgur.uploadFile(files.image.path)
                            .then(function (json) {
                                var link = json.data.link
                                var deletehash = json.data.deletehash
                                // Post avec une photo
                                resolve(new PostSchema({
                                    created_by: decoded_token.user.username,
                                    content: {
                                        title: fields.title,
                                        desc: fields.desc || null,
                                        pic_link: link,
                                        deletehash: deletehash
                                    }
                                }));
                            })
                            .catch(function (err) {
                                res.status(512).send({success: false, msg: err.message, severity: "danger"})
                        })
                    })
                }
            }
            else if (fields.title && fields.desc) {
                // Pas d'image
                    post = new Promise(function (resolve) {
                        resolve(new PostSchema({
                            created_by: decoded_token.user.username,
                            content: {
                                title: fields.title,
                                desc: fields.desc
                            }
                        }));
                    })
            } else {
                res.status(Error.e03.http_status).send(Error.e03)
                var post = "bad"
            }
            if (post != "bad") {
                Promise.all([post]).then(function (value) {
                    value = value[0]
                    if (value) {
                        // Verifie le deriner post de l'utilisateur
                        // Il doit y avoir 45 sec entre chaque posts
                        User.aggregate([{$match:{username: decoded_token.user.username}},
                            {$unwind:"$posts"},
                            {$sort:{"posts.post_time": -1}},
                            {$limit: 1},
                            {$project: {posts:1}}], function (err, user_posts) {
                            user_posts = user_posts[0] // Retire du tableau. C'est sale mais blc
                            if (err){
                                res.status(Error.e99.http_status).send(Error.e99)
                            } else if (user_posts && (functions.getEpoch() - user_posts.posts.post_time < 45)){
                                // Si moins de 5 minutes entre les posts
                                // Si il y a deja eu un post
                                res.status(Error.e47.http_status).send({
                                    success: false,
                                    msg: "Merci d'attendre 45 secondes entre chaques posts.",
                                    err_code: 47,
                                    temps_restant: (functions.getEpoch() - user_posts.posts.post_time - 45),
                                    severity: "warning",
                                    http_status: 403
                                })
                            }else {
                                // Sinon all good
                                value.save(function (err, post) {
                                    if (err || !post) {
                                        // Error during process, try again later
                                        res.status(Error.e37.http_status).send(Error.e37)
                                    } else {
                                        User.findOneAndUpdate({username: decoded_token.user.username},
                                            {$push: {posts: {post_id: post._id}}}, function (err, user) {
                                                // Ajout du post au compte
                                                if (err || !user) {
                                                    // Remove post
                                                    PostSchema.findOneAndRemove({_id: post._id}, function (err, rem) {
                                                        res.status(Error.e99.http_status).send(Error.e99)
                                                    })
                                                } else {
                                                    // Success !
                                                    res.status(201).send({
                                                        success: true,
                                                        msg: "Post created",
                                                        post: post
                                                    })
                                                }

                                            })
                                    }
                                })
                            }
                        })
                    }

                }).catch(function (error) {
                    res.status(512).send({succes: false, msg: error})
                })
            }
        })

    })

    app.get(apiconfig.api_url + 'post/:id', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        // Trouve un post dans la bdd
        // Si utilisateur trouvé et pas d'erreur
        var decoded_token = functions.getDecodedToken(req.headers)
        if (req.params.id && decoded_token) {
            // Si l'id du post en ok
            // Retun le post demandé
            PostSchema.findOne({_id: req.params.id},{__v:0}, function (err, post) {
                if (!post) {
                    res.status(Error.e40.http_status).send(Error.e40)
                } else {
                    // Savoir si l'utilisateur à deja voté pour ce post
                    User.findOne({
                        $and: [
                            {username: decoded_token.user.username},
                        ],
                        voted_posts: {$elemMatch: {post_id: req.params.id}}
                    }, {'voted_posts.$': 1})
                        .select("voted_posts")
                        .exec(function (err, vote) {
                            User.findOne(User.findOne({username: post.created_by}, {"points_elite": 1, "username": 1, "gender": 1, "birthday": 1, "avatar_link": 1}),
                                function (err, user) {
                                    if (err) {
                                        res.status(Error.e99.http_status).send(Error.e99)
                                    } else {
                                        res.status(200).send({success: true, post: post, vote: vote, user: user})
                                    }
                            })

                        })
                }
            })
        } else {
            res.status(Error.e03.http_status).send(Error.e03)
        }

    })

    app.get(apiconfig.api_url + 'posts/:page?', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        // Récupere les posts
        // Si utilisateur trouvé et pas d'erreur
        PostSchema.paginate({}, {
            sort: {created_at: -1},
            select: {_id: 1},
            limit: 5,
            page: req.params.page || 1
        }, function (err, posts) {
            if (err) {
                res.status(Error.e99.http_status).send(Error.e99)
            } else if (!posts) {
                // Return : pas de contenu
                res.status(Error.e38.http_status).send(Error.e38)
            } else {
                res.status(200).send({success: true, posts: posts})
            }
        })


    })

    app.post(apiconfig.api_url + 'user/vote', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        // Vote pour un post
        var decoded_token = functions.getDecodedToken(req.headers);
        // Si utilisateur trouvé et pas d'erreur
        // Verification des champs
        if (req.body.post_id && req.body.vote && decoded_token) {
            // Cherche si le post existe
            PostSchema.findOne({_id: req.body.post_id}, function (err, post) {
                if (err || !post) {
                    // Si erreur ou si le post n'existe pas
                    res.status(Error.e40.http_status).send(Error.e40)
                }
                // Cherche si l'utilasteur veur voter pour lui meme
                else if (post.created_by === decoded_token.user.username){
                    res.status(Error.e43.http_status).send(Error.e43)
                }
                else {
                    // Cherche si le post n'est pas deja dans la base
                    // Transforme le vote en boolean
                    if (req.body.vote === "up") {
                        var vote = true
                        var mouv = 1
                        var c_mouv = -1
                        var u_mouv = 2 // Si update, alors retier le moins 1 et ajouter 1
                    } else if (req.body.vote === "down") {
                        var vote = false
                        var mouv = -1
                        var c_mouv = 1
                        var u_mouv = -2
                    } else {
                        // Si bad request
                        var vote = "bad"
                    }
                    // Si la requete est bonne
                    // Trois états :
                    // Pas de vote --> Creation vote et +/-1 sur le post
                    // Vote different --> Change les valeurs
                    // Même vote --> Annule le vote

                    if (vote !== "bad") {
                        User.findOne({
                            username: decoded_token.user.username,
                            $and: [
                                {voted_posts: {$elemMatch: {post_id: req.body.post_id}}},
                            ],

                        }, {'voted_posts.$': 1}, function (err, voted_post) {
                            // User table update
                            if (err) {
                                // Si erreur
                                res.status(Error.e99.http_status).send(Error.e99)
                                return

                            } else if (!voted_post) {
                                // console.log("Non trouvé")
                                var createur_update = [{username: post.created_by}, {$inc: {points_elite: mouv}}]
                                var post_update = [{_id: req.body.post_id}, {$inc: {points_elite: mouv}}]
                                // Si le post n'est pas deja voté, ajouter au compte
                                var user_update = new Promise(function (resolve) {
                                    resolve(User.findOneAndUpdate({username: decoded_token.user.username},
                                        {$push: {voted_posts: {post_id: req.body.post_id, vote: vote}}}
                                    ).select('username')
                                        .select('voted_posts')
                                        .exec())
                                });
                            } else if (voted_post.voted_posts[0].vote === vote) {
                                // Le vote est le meme alors annule le vote
                                //console.log("Annulation")
                                var action = "cancel"
                                var createur_update = [{username: post.created_by}, {$inc: {points_elite: c_mouv}}]
                                var post_update = [{_id: req.body.post_id}, {$inc: {points_elite: c_mouv}}]
                                var user_update = new Promise(function (resolve) {
                                    resolve(User.update({_id: voted_post['_id']}, {$pull: {voted_posts: {_id: voted_post['voted_posts'][0]['_id']}}}));
                                })
                            } else {
                                // Si le post est deja voté, changer la valeur sur le compte
                                // console.log("Trouvé")
                                var createur_update = [{username: post.created_by}, {$inc: {points_elite: u_mouv}}]
                                var post_update = [{_id: req.body.post_id}, {$inc: {points_elite: u_mouv}}]
                                var user_update = new Promise(function (resolve) {
                                    resolve(User.findOneAndUpdate({username: decoded_token.user.username},
                                        {$set: {"voted_posts.$[elem].vote": vote}},
                                        {arrayFilters: [{"elem.post_id": req.body.post_id}]})
                                        .select('username')
                                        .select('voted_posts')
                                        .exec())
                                });
                            }
                            // Post table update
                            var post_update = new Promise(function (resolve) {
                                // Ajoute ou retire un point au post
                                resolve(PostSchema.findOneAndUpdate(post_update[0], post_update[1]))
                            })
                            var createur_update = new Promise(function (resolve) {
                                // ajoute ou retire des points élite à l'utilisateur
                                resolve(User.findOneAndUpdate(createur_update[0], createur_update[1]))
                            })
                            Promise.all([user_update]).then(function (value) {
                                res.status(200).send({success: true, action: action || "vote", doc: value})
                            }).catch(function (error) {
                                res.status(512).send({success: false, doc: error})
                            })
                        })
                    } else {
                        // Si le vote n'est pas bien formaté, return 400
                        res.status(Error.e39.http_status).send(Error.e39);
                    }
                }
            });
        } else {
            res.status(Error.e03.http_status).send(Error.e03)
        }

    })

    app.post(apiconfig.api_url + 'user/changepassword', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
                // si l'utilisateur est auth
        var decoded_token = functions.getDecodedToken(req.headers);

        if (req.body.old_pwd && req.body.new_pwd && decoded_token) {
            User.findOne({
                $and: [
                    {username: decoded_token.user.username},
                    {password: decoded_token.user.password}
                ]
            }, function (err, user) {
                if (err || !user){
                    res.status(Error.e99.http_status).send(Error.e99)
                }
                else if (req.body.new_pwd !== req.body.old_pwd) {
                    user.comparePassword(req.body.old_pwd, function (err, isMatch) {
                        if (isMatch && !err) {
                            // Si l'ancien mdp ok. procéder au chagement
                            bcrypt.hash(req.body.new_pwd, bcrypt.genSaltSync(BCRYPT_SALT_ROUNDS), null, function (err, hash) {
                                if (err) {
                                    console.log(err);
                                    res.status(Error.e13.http_status).send(Error.e13)
                                } else {
                                    User.findOneAndUpdate({
                                            $and: [
                                                {username: decoded_token.user.username},
                                                {password: decoded_token.user.password}
                                            ]
                                        },
                                        {password: hash}, function (err, doc) {
                                            if (err) {
                                                res.status(Error.e99.http_status).send(Error.e99, {
                                                    msg: err,
                                                    doc: doc
                                                })
                                            }else {
                                                res.status(200).send({
                                                    success: true,
                                                    msg: "Password successfully modified."
                                                });
                                            }
                                        });
                                }
                            });


                        } else {
                            res.status(Error.e12.http_status).send(Error.e12)
                        }
                    });
                } else {
                    res.status(Error.e14.http_status).send(Error.e14)
                }
            });
        } else {
            res.status(Error.e03.http_status).send(Error.e03)
        }
    });

    app.get(apiconfig.api_url + 'user', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        // fonction autosuffisante
        var decoded_token = functions.getDecodedToken(req.headers);
        if (decoded_token) {
            User.findOne(
                {
                    username: decoded_token.user.username,
                    $and: [{password: decoded_token.user.password}]
                }, function (err, user) {
                    if (!user || err) {
                        return res.status(401).send({success: false, msg: "Auth failure. User not found"})
                    } else {
                        // User authentified.
                        res.status(200).send({
                            points_elite: user.points_elite,
                            CAEL_id: decoded_token.card.CAEL_id,
                            username: user.username,
                            card_type: decoded_token.card.type,
                            departement: decoded_token.card.departement,
                            exp_date: decoded_token.card.exp_date,
                            success: true,
                            lastname: user.lastname,
                            surname: user.surname,
                            admin: user.admin,
                            avatar_link: user.avatar_link
                        })
                    }
                })
        } else {
            return res.status(Error.e10.http_status).send(Error.e10);
        }
    });

    app.get(apiconfig.api_url + 'user/get_user/:username', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        // Get info of a id
        try {
            if ((req.params.username).length >= 3) {
                var card_infos = new Promise(function (resolve) {
                    resolve(CardsTable.findOne({
                        user_id: req.params.username,
                        $and: [{revoked: false}]
                    }, {"CAEL_id": 1, "user_id": 1, "type": 1, "departement": 1}))
                })
                var user_infos = new Promise(function (resolve) {
                    resolve(User.findOne({username: req.params.username}, {"points_elite": 1, "username": 1, "gender": 1, "birthday": 1, "avatar_link": 1}))
                })

                Promise.all([card_infos, user_infos])
                    .then(function (values) {
                        if (values) {
                            res.send({success: true, card_infos: values[0], user_infos: values[1]})
                        } else {
                            res.status(Error.e04.http_status).send(Error.e04)
                        }
                    })
                    .catch(function (err) {
                        res.status(Error.e99.http_status).send({msg: Error.e99, err: err})
                    });
            } else {
                res.status(Error.e16.http_status).send(Error.e16)
            }
        } catch (e) {
            res.status(Error.e99.http_status).send(Error.e99 + e)
        }
    });

    app.post(apiconfig.api_url + 'user/changecard', passport.authenticate('jwt', {session: false}), function (req, res) {
        // Fontion autosuffisante
        var decoded_token = functions.getDecodedToken(req.headers);
        if (decoded_token) {
            User.findOne({
                $and: [
                    {username: decoded_token.user.username},
                    {password: decoded_token.user.password}
                ]
            }, function (err, user) {
                if (user) {
                    if (req.body.CAEL_id && req.body.code) {
                        CardsTable.findOne({
                            CAEL_id: req.body.CAEL_id,
                            // Si la carte exite
                            $and: [
                                {
                                    CAEL_code: req.body.code
                                    // Et si le code correspond
                                }]
                        }, function (err, card) {
                            if (err) throw err;
                            if (card) {
                                // Carte trouvée
                                // Verification si elle n'est pas révoquée
                                if (card.revoked) {
                                    res.status(Error.e23.http_status).send(Error.e23);
                                } else if (card.assign || card.user_id) {
                                    res.status(Error.e25.http_status).send(Error.e25)
                                } else {
                                    // Verif ok, desactivation de l'ancienne carte (revoqué)
                                    CardsTable.findOneAndUpdate({$and: [{user_id: decoded_token.user.username}, {revoked: false}]}, {revoked: true}, function (err, docs) {
                                        if (err || !docs) res.status(Error.e99.http_status).send(Error.e99)
                                        else {
                                            // Activation de la nouvelle carte
                                            CardsTable.findOneAndUpdate({$and: [{CAEL_id: req.body.CAEL_id}, {CAEL_code: req.body.code}]}, {
                                                assign: true,
                                                user_id: decoded_token.user.username
                                            }, function (err, act) {
                                                if (err || !act) {
                                                    // Si erreur, rollback
                                                    CardsTable.findOneAndUpdate({user_id: decoded_token.user.username}, {revoked: false}, function (err, doc) {
                                                        res.status(Error.e08.http_status).send(Error.e08);
                                                    })
                                                } else {
                                                    // All good !
                                                    res.status(Error.e32.http_status).send({
                                                        success: true,
                                                        msg: "Card " + card.CAEL_id + " successfully changed!",
                                                        card_type: card.type,
                                                        departement: card.departement,
                                                        exp_date: card.exp_date
                                                    });
                                                }
                                            })
                                        }
                                    })
                                }
                            } else {
                                res.status(Error.e24.http_status).send(Error.e24)
                            }
                        });
                    } else {
                        res.status(Error.e03.http_status).send(Error.e03)
                    }

                } else {
                    res.status(Error.e06.http_status).send(Error.e06)
                }
            })
        } else {
            res.status(Error.e10.http_status).send(Error.e10);
        }
    })

    app.post(apiconfig.api_url + 'token/check', isAuth, passport.authenticate('jwt', {session: false}), function (req, res) {
        var token = functions.getToken(req.headers);
        var decoded_token = jwt.verify(token, config.secret);
        res.status(200).send({success: true, result: token});
    })
}
