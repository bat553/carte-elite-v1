/*
    api
    CAEL

    Created by Swano 21/01/19 20:22

    Variables Api
*/

module.exports = {
    api_base : "api",
    api_version : "v3",
    api_url : "/api/v3/"
}

