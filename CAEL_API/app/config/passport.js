var passport = require('passport')
    JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt,
    error = require('../models/errors').ErrorList;

var User = require('../models/user');
var config = require('../config/db'); // get db config file

var params = {
    secretOrKey: config.secret,
    jwtFromRequest : ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
    failureRedirect: '/login',
    failureFlash: true,
}

module.exports = function() {
    passport.use(new JwtStrategy(params, function(jwt_payload, done) {
        User.findOne({id: jwt_payload.id}, function(err, user) {
            if (err) {
                return done(err, false);
            }
            if (user) {
                return done(null, user);
            } else {
                return done(null, false, error.e11);
                // or you could create a new account
            }
        });
    }));
};