// On lance le serveur node à tester
// https://tech.m6web.fr/introduction-qualite-logicielle-avec-node-js
//var server = require('../server.js'); // Activate while deploying
var request = require('supertest');
var request = request('http://localhost:8080');
var baseUrl = "/api/v3/";
var infinite_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InBvaW50c19lbGl0ZSI6MCwiZGlzYWJsZSI6ZmFsc2UsIl9pZCI6IjVjNjAyMDdlZGU5MWY2MDNkZGE2MTIzZSIsInVzZXJuYW1lIjoiZGV2X2FjYyIsInBhc3N3b3JkIjoiJDJhJDEwJC9VL0VuaDBPVW4wekFpRmhwTnJEOE9TWUEvSnlKLzRLQmYxL3ZKclMzRVpqSllSRmdqYkVXIiwiZW1haWwiOiJqZWFuQGNvdHR5LmZyIiwibGFzdG5hbWUiOiJERVYiLCJzdXJuYW1lIjoiQUNDT1VOVCIsImFkbWluIjp0cnVlLCJnZW5kZXIiOiJoIiwiYmlydGhkYXkiOiIxOC8xMS8yMDAwIiwicmVnaXN0cmF0aW9uX3RpbWUiOjE1NDk4MDM2NDYsImxhc3RMb2dpbiI6MTU0OTgxNzg1MX0sImNhcmQiOnsicmV2b2tlZCI6ZmFsc2UsImFzc2lnbiI6dHJ1ZSwidHlwZSI6ImdvbGQiLCJkZXBhcnRlbWVudCI6IlJUIiwiQ0FFTF9pZCI6IjE5MDUwMDA4IiwicmVnaXN0cmF0aW9uX3RpbWUiOjE1NDk1NTk3NTksImV4cF9kYXRlIjoxNTgxMDk1NzU5LCJ1c2VyX2lkIjoiZGV2X2FjYyJ9LCJpYXQiOjE1NDk4MTgwMjcsImV4cCI6MTYwMDAwMDAwMH0.UL5yEkSxzRXpBHeM1duGEWh1nSTYYtfcfH6MLsZwPSM"

describe('Test backend API (Api only)' , function () {
    it('return 200 when request public/stats', function(done) {
        request
            .get( baseUrl +'public/stats')
            .expect(200)
            .end(done);
    });
    it('test auth, using dev_acc, should return 200 ', function (done) {
        request
            .post(baseUrl + 'auth')
            .send({
                    username: "dev_acc",
                    password: "234567891!"
                })
            .expect(200)
            .end(done);
    });
    it('test get feed, using infinite_token, should return 200', function (done) {
        request
            .get(baseUrl + "posts")
            .set("Authorization", "Bearer "+ infinite_token)
            .expect(200)
            .end(done);
    });

    it('test user details, using infinite_token, should return 200', function (done) {
        request
            .get(baseUrl + "user")
            .set("Authorization", "Bearer "+ infinite_token)
            .expect(200)
            .end(done);
    });
    it('test get user, using infinite_token, should return 200', function (done) {
        request
            .get(baseUrl + "user/get_user/dev_acc")
            .set("Authorization", "Bearer "+ infinite_token)
            .expect(200)
            .end(done);
    });

    it('test get quote,  should return 200', function (done) {
        request
            .get(baseUrl + "quote")
            .expect(200)
            .end(done);

    });

    it('test get_cards, using infinit_token, should return 200', function (done) {
        request
            .get(baseUrl + "admin/cards/")
            .set("Authorization", "Bearer "+ infinite_token)
            .expect(200)
            .end(done)

    });
    it('test get_card, using infinit_token, should return 200', function (done) {
        request
            .get(baseUrl + "admin/card/19050008")
            .set("Authorization", "Bearer "+ infinite_token)
            .expect(200)
            .end(done)

    })
    it('test check/token, using infinit_token, should return 200', function (done) {
        request
            .post(baseUrl + "token/check")
            .set("Authorization", "Bearer "+ infinite_token)
            .expect(200)
            .end(done)

    })
    it('test get_users, using infinit_token, should return 200', function (done) {
        request
            .get(baseUrl + "admin/users/list")
            .set("Authorization", "Bearer "+ infinite_token)
            .expect(200)
            .end(done)

    });

});